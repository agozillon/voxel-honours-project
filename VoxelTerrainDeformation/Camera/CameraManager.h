#pragma once

#include <map>
#include <string>
#include <memory>
#include <glm/gtc/type_ptr.hpp>

#include "Camera.h"

enum CameraType
{
	FIRSTPERSONCAMERA
};

// camera manager allows you to add and switch between multiple cameras (should be possible to easily add different types as well)
// USED IN EXTERNAL PROJECT, SIMILAR TO 3RD YEAR GROUP PROJECT
class CameraManager
{

public:
	CameraManager(); // constructor that creates a "Default" Camera 
	~CameraManager(){} // blank inline destructor 
	void updateEye(float x, float y, float z){ m_cameraList[m_activeCamera]->updateEye(x, y, z); }		       // inline function that updates the Eye position via a passed in set of floats 
	void updateAt(float x, float y, float z){ m_cameraList[m_activeCamera]->updateAt(x, y, z); }		       // inline function that updates the At position via a passed in set of floats 
	void updateUp(float x, float y, float z){ m_cameraList[m_activeCamera]->updateUp(x, y, z); }	           // inline function that updates the Up position via a passed in set of floats
	void updatePitch(float rot){ m_cameraList[m_activeCamera]->updatePitch(rot); }				               // inline function that updates the cameras pitch 
	void updateYaw(float rot){ m_cameraList[m_activeCamera]->updateYaw(rot); }								   // inline function that updates the cameras rotation
	void updateAspectRatio(float aspectRatio){ m_cameraList[m_activeCamera]->updateAspectRatio(aspectRatio); } // inline function that updates the cameras aspect ratio
	
	// all inline return functions that call the active camera and return the various 
	// variables
	float getYaw() { return m_cameraList[m_activeCamera]->getYaw(); }												    // inline function that returns the cameras yaw as a float
	float getPitch() { return m_cameraList[m_activeCamera]->getPitch(); }												// inline function that returns the cameras pitch as a float
	float getFOV() { return m_cameraList[m_activeCamera]->getFOV(); }													// inline function that returns the cameras field of view as a float
	float getNear() { return m_cameraList[m_activeCamera]->getNear(); }												    // inline function that returns the cameras near cutting distance as a float
	float getFar() { return m_cameraList[m_activeCamera]->getFar(); }													// inline function that returns the cameras far cutting distance as a float
	float getAspectRatio() { return m_cameraList[m_activeCamera]->getAspectRatio(); }									// inline function that returns the cameras aspect ratio as a float
	mat4 getView() { return m_cameraList[m_activeCamera]->getView(); }													// inline function that returns the cameras LookAt as a mat4
	mat4 getProjection() { return m_cameraList[m_activeCamera]->getProjection(); }										// inline function that returns the cameras Projection as a mat4
	vec3 getEye() { return m_cameraList[m_activeCamera]->getEye(); }													// inline function that returns the cameras Eye position as a vec3
	vec3 getAt() { return m_cameraList[m_activeCamera]->getAt(); }														// inline function that returns the cameras At position as a vec3
	vec3 getUp() { return m_cameraList[m_activeCamera]->getUp(); }														// inline function that returns the cameras Up position as a vec3

	// inline function that calls the cameras move function, calls an empty function if the camera type has no implementation
	void move(float distRight, float distUp, float distForward){ m_cameraList[m_activeCamera]->move(distRight, distUp, distForward); }

	// Strictly manager functions, one adds a camera to the manager, the key is the passed in name
	// and the CameraType is what gets created. The other switches to the camera with the specified name
	void addCamera(const char* cameraName, CameraType);
	void switchCamera(const char* cameraName); 

private:
	// map for holding the cameras 
	std::map<std::string, std::shared_ptr<Camera>> m_cameraList;
	std::string m_activeCamera;

};

