#include "FirstPersonCamera.h"

#define DEG_TO_RADIAN 0.017453293

// constructor that sets default values up for the FirstPersonCamera
FirstPersonCamera::FirstPersonCamera() : m_yaw(0), m_pitch(0), m_aspectRatio(800 / 600), m_fov(45), m_near(1), m_far(300)
{
	// calling the update functions to make the constructor shorter
	updateEye(0.0, 0.0, 0.0);
	updateAt(0.0, 0.0, -1.0);
	updateUp(0.0, 1.0, 0.0);
}

// updates the pitch of the camera up or down, locks it to a certain value
// so that it doesn't mess around with move up or down or cause the up vectortochange. 
void FirstPersonCamera::updatePitch(float rot)
{
	m_pitch += rot;

	if (m_pitch > 70)
		m_pitch = 70;
	else if (m_pitch <= -70)
		m_pitch = -70;

	m_at = glm::vec3(m_eye.x + 1.0*std::sin(m_yaw*DEG_TO_RADIAN), m_eye.y + 1.0 * std::tan(m_pitch*DEG_TO_RADIAN), m_eye.z - 1.0*std::cos(m_yaw*DEG_TO_RADIAN));
}

// rotates the camera right or left by rotating the position the camera is staring
// at via the moveForward function and a passed in rotation value
void FirstPersonCamera::updateYaw(float rot)
{
	m_yaw += rot;
	m_at = glm::vec3(m_eye.x + 1.0*std::sin(m_yaw*DEG_TO_RADIAN), m_eye.y + 1.0 * std::tan(m_pitch*DEG_TO_RADIAN), m_eye.z - 1.0*std::cos(m_yaw*DEG_TO_RADIAN));
}

// encapsulates movement if you wish to move in one direction use 1, 0, 0 for forward movement etc.
void FirstPersonCamera::move(float distRight, float distUp, float distForward)
{
	if (distForward != 0)
		moveForwardOrBackwards(distForward);

	if (distRight != 0)
		moveRightOrLeft(distRight);

	if (distUp != 0)
		moveUpOrDown(distUp);
}

// moves the camera forward or backwards using a passed in distance
// for the eye and using the eye value and a defualt 1 value to move the at
// so the camera is always looking a set distance ahead
void FirstPersonCamera::moveForwardOrBackwards(float dist)
{
	m_eye = glm::vec3(m_eye.x + dist*std::sin(m_yaw*DEG_TO_RADIAN), m_eye.y + dist * std::tan(m_pitch*DEG_TO_RADIAN), m_eye.z - dist*std::cos(m_yaw*DEG_TO_RADIAN));
	m_at = glm::vec3(m_eye.x + 1.0*std::sin(m_yaw*DEG_TO_RADIAN), m_eye.y + 1.0 * std::tan(m_pitch*DEG_TO_RADIAN), m_eye.z - 1.0*std::cos(m_yaw*DEG_TO_RADIAN));
}

// moves the camera right or left using a passed in distance
// for the eye and move right and using the eye value and a defualt 1 
// value with the moveForward function to move the at so the camera is 
// always looking a set distance ahead of the eye
void FirstPersonCamera::moveRightOrLeft(float dist)
{
	m_eye = glm::vec3(m_eye.x + dist*std::cos(m_yaw*DEG_TO_RADIAN), m_eye.y, m_eye.z + dist*std::sin(m_yaw*DEG_TO_RADIAN));
	m_at =  glm::vec3(m_eye.x + 1.0*std::sin(m_yaw*DEG_TO_RADIAN), m_eye.y + 1.0 * std::tan(m_pitch*DEG_TO_RADIAN), m_eye.z - 1.0*std::cos(m_yaw*DEG_TO_RADIAN));
}

// moves the camera right or left using a passed in distance
// for the eye and move right and using the eye value and a defualt 1 
// value with the moveForward function to move the at so the camera is 
// always looking a set distance ahead of the eye
void FirstPersonCamera::moveUpOrDown(float dist)
{
	m_eye = glm::vec3(m_eye.x, m_eye.y+dist, m_eye.z);
	m_at = glm::vec3(m_eye.x + 1.0*std::sin(m_yaw*DEG_TO_RADIAN), m_eye.y + 1.0 * std::tan(m_pitch*DEG_TO_RADIAN), m_eye.z - 1.0*std::cos(m_yaw*DEG_TO_RADIAN));
}





