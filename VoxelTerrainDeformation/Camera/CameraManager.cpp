#include "CameraManager.h"
#include "FirstPersonCamera.h"

#include <iostream>

CameraManager::CameraManager()
{
	// Basically adding a "Null" if you will, camera onto the map so that 
	// if someone doesn't initially add a camera it'll not crash when a function
	// is pre-called, it'll just feedback a default FPS camera that can be used(if you want)
	m_cameraList["Default"] = std::shared_ptr<FirstPersonCamera>(new FirstPersonCamera());
	m_activeCamera = "Default";

	std::cout << "Camera Manager Created" << std::endl;
}

// Simply adds a camera of the specified type to the CameraManager, tried to leave it open to extendabillity
// for different types of cameras.
void CameraManager::addCamera(const char* cameraName, CameraType type)
{
	switch (type)
	{
		case FIRSTPERSONCAMERA: // creates a first person camera and sets it as the active camera
			m_cameraList[cameraName] = std::shared_ptr<FirstPersonCamera>(new FirstPersonCamera());
			m_activeCamera = cameraName;
			break;
	    default:
			std::cout << "Incorrect Camera Type" << std::endl;
			break;
	}
	
	
}

// switches the current camera to the camera with the specified name 
void CameraManager::switchCamera(const char* cameraName)
{

#ifdef _DEBUG
	if (m_cameraList.find(cameraName) == m_cameraList.end())
	{
		std::cout << "No camera with this name in the list!" << std::endl;
		return;
	}
#endif

	m_activeCamera = cameraName;
}