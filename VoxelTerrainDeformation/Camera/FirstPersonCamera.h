#pragma once

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Camera.h"

// basic First Person Camera class that inherits from Camera and can be used to move a 3D Perspective View camera around a scene
// doesn't use quaternians uses some fairly simple euler functions. USED IN EXTERNAL PROJECT, SIMILAR TO 3RD YEAR GROUP PROJECT/RT3D/GED/AGP etc.
class FirstPersonCamera: public Camera
{

public:
	FirstPersonCamera();																	 // constructor sets up default values for the first person camera
	~FirstPersonCamera(){}																	 // blank inline destructor
	void updateEye(float x, float y, float z){ m_eye.x = x; m_eye.y = y; m_eye.z = z; }		 // inline function that updates the Eye position via a passed in set of floats 
	void updateAt(float x, float y, float z){ m_at.x = x; m_at.y = y; m_at.z = z; }			 // inline function that updates the At position via a passed in set of floats 
	void updateUp(float x, float y, float z){ m_up.x = x; m_up.y = y; m_up.z = z; }			 // inline function that updates the Up position via a passed in set of floats
	void updatePitch(float rot);															 // function that updates the cameras pitch and updates the camera
	void updateYaw(float rot);																 // function that updates the cameras rotation and updates the camera
	void updateAspectRatio(float aspectRatio){ m_aspectRatio = aspectRatio; }			     // inline function that updates aspect ratio of the camera
	
	float getYaw() const { return m_yaw; }														// inline function that returns the cameras yaw as a float
	float getPitch() const { return m_pitch; }													// inline function that returns the cameras pitch as a float
	float getFOV() const { return m_fov; }														// inline function that returns the cameras fov as a float	
	float getNear() const { return m_near; }													// inline function that returns the cameras near as a float		
	float getFar() const { return m_far; }														// inline function that returns the cameras far as a float
	float getAspectRatio() const { return m_aspectRatio; }										// inline function that returns the cameras aspect ratio as a float
	mat4 getView() const { return glm::lookAt(m_eye, m_at, m_up); }								// inline function that returns the cameras LookAt(view matrix) as a mat4
	mat4 getProjection() const { return glm::perspective(m_fov, m_aspectRatio, m_near, m_far); }// inline function that returns the cameras projection matrix
	vec3 getEye() const { return m_eye; }														// inline function that returns the cameras Eye position as a vec3
	vec3 getAt() const { return m_at; }														    // inline function that returns the cameras At position as a vec3
	vec3 getUp() const { return m_up; }													    	// inline function that returns the cameras Up position as a vec3
	
	// move function that moves the camera left, right, up, down, forward or backwards.
	// Rotation is based off of the pitch and yaw variables.
	void move(float distRight, float distUp, float distForward);
	
private:
	void moveForwardOrBackwards(float dist);				   // function used to move the camera forwards or backwards by a passed in distance
	void moveRightOrLeft(float dist);						   // function used to move the camera left or right by a passed in distance
	void moveUpOrDown(float dist);							   // function used to move the camera up or down by a passed in distance

	// variables to hold the cameras Rotation/Eye/At/Up  
	float m_yaw, m_pitch, m_fov, m_near, m_far, m_aspectRatio;
	vec3 m_eye, m_at, m_up;

};
