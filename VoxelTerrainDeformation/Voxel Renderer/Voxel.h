#pragma once
#include <GL\glew.h>
#include <memory>

#include "../Camera/CameraManager.h"
#include "../Shader Manager/ShaderManager.h"
#include "../Collision Detection/BoundingBox.h"

class Voxel
{

public:
	Voxel(bool isVisible){ m_isVisible = isVisible; m_voxelCount += 1; }
	Voxel(bool isVisible, glm::vec3 position, glm::vec3 dimenison);
	~Voxel(){}
	bool getVisible(){ return m_isVisible; }
	void updateVisible(bool visible){ m_isVisible = visible; }
	static int getVoxelCount(){ return m_voxelCount; }
	std::shared_ptr<BoundingBox> getCollisionBox(){ return m_collisionBox; }

private:
	// Per voxel variables
	bool m_isVisible;
	std::shared_ptr<BoundingBox> m_collisionBox;

	// Shared Variables for memory saving
	static int m_voxelCount; 
};