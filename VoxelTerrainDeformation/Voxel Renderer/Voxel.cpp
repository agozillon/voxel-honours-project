#include "Voxel.h"

int Voxel::m_voxelCount = 0;

Voxel::Voxel(bool isVisible, glm::vec3 position, glm::vec3 dimenison)
{ 
	m_isVisible = isVisible;
	m_voxelCount += 1;
	m_collisionBox = std::shared_ptr<BoundingBox>(new BoundingBox(position, dimenison)); 
}