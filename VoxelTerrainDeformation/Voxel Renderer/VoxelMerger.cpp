#include "VoxelMerger.h"


void VoxelMerger::pushPolygon(glm::vec3 vert1, glm::vec3 vert2, glm::vec3 vert3, glm::vec2 uv1, glm::vec2 uv2, glm::vec2 uv3)
{
	// front
	m_mergedMesh.push_back(vert1.x); m_mergedMesh.push_back(vert1.y); m_mergedMesh.push_back(vert1.z);
	m_mergedMesh.push_back(vert2.x); m_mergedMesh.push_back(vert2.y); m_mergedMesh.push_back(vert2.z);
	m_mergedMesh.push_back(vert3.x); m_mergedMesh.push_back(vert3.y); m_mergedMesh.push_back(vert3.z);
	m_mergedUV.push_back(uv1.x); m_mergedUV.push_back(uv1.y);
	m_mergedUV.push_back(uv2.x); m_mergedUV.push_back(uv2.y);
	m_mergedUV.push_back(uv3.x); m_mergedUV.push_back(uv3.y);
}

void VoxelMerger::addVoxel(glm::vec3 position, float blockSize)
{
	// Calculate 8 points of the cube, point1 = front left bottom, point2 = front right bottom, 
	// point 3 = front right top, point 4 =  front left top, point 5 = back right bottom
	// point 6 = back left bottom, point 7 = back left top, point 8 = back right top
	glm::vec3 point1(position.x - blockSize, position.y - blockSize, position.z + blockSize);
	glm::vec3 point2(position.x + blockSize, position.y - blockSize, position.z + blockSize);
	glm::vec3 point3(position.x + blockSize, position.y + blockSize, position.z + blockSize);
	glm::vec3 point4(position.x - blockSize, position.y + blockSize, position.z + blockSize);
	glm::vec3 point5(position.x + blockSize, position.y - blockSize, position.z - blockSize);
	glm::vec3 point6(position.x - blockSize, position.y - blockSize, position.z - blockSize);
	glm::vec3 point7(position.x - blockSize, position.y + blockSize, position.z - blockSize);
	glm::vec3 point8(position.x + blockSize, position.y + blockSize, position.z - blockSize);

	glm::vec2 uv1(0.0f, 1.0f);
	glm::vec2 uv2(0.0f, 0.0f);
	glm::vec2 uv3(1.0f, 1.0f);
	glm::vec2 uv4(1.0f, 0.0f);

	// Defines cube faces
	// front
	pushPolygon(point1, point3, point4, uv2, uv3, uv1);
	pushPolygon(point1, point2, point3, uv2, uv4, uv3);

	// back
	pushPolygon(point5, point7, point8, uv2, uv3, uv1);	
	pushPolygon(point5, point6, point7, uv2, uv4, uv3);

	// right
	pushPolygon(point2, point8, point3, uv4, uv1, uv3);
	pushPolygon(point2, point5, point8, uv4, uv2, uv1);
	
	// Left
	pushPolygon(point6, point1, point4, uv2, uv4, uv3);
	pushPolygon(point6, point4, point7, uv2, uv3, uv1);

	// Top
	pushPolygon(point4, point3, point8, uv1, uv3, uv4);
	pushPolygon(point4, point8, point7, uv1, uv4, uv2);
	
	// Bottom
	pushPolygon(point6, point5, point2, uv1, uv3, uv4);
	pushPolygon(point6, point2, point1, uv1, uv4, uv2);
}

void VoxelMerger::createMesh(GLuint &vao, GLuint& indiceCount)
{
	if (m_mergedMesh.size() > 0) // Only try create a mesh if data actually exists!
	{
		// bind VAO
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		bindVBO(&m_mergedMesh[0], 0, GL_ARRAY_BUFFER, GL_STREAM_DRAW, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*m_mergedMesh.size(), 3);
		bindVBO(&m_mergedUV[0], 1, GL_ARRAY_BUFFER, GL_STREAM_DRAW, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*m_mergedUV.size(), 2);

		indiceCount = m_mergedMesh.size() / 3;

		m_mergedMesh.clear();
		m_mergedUV.clear();
	}
}

GLuint VoxelMerger::bindVBO(const GLvoid* data, GLint attribIndex, GLenum targetType, GLenum drawMode, GLenum dataType, GLboolean normalized, size_t sizeOfData, GLuint numberOfDataPerValue)
{
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(targetType, vbo);
	glBufferData(targetType, sizeOfData, data, drawMode);

	// if it has a -ve attrib index then its not a normal attribute type, such as indice values that use GL_ELEMENT_ARRAY_BUFFER!
	if (attribIndex >= 0)
	{
		glEnableVertexAttribArray(attribIndex);
		glVertexAttribPointer(attribIndex, numberOfDataPerValue, dataType, normalized, 0, 0);
	}

	return vbo;
}