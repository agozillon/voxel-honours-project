#pragma once
#include <memory>

#include "../Collision Detection/BoundingBox.h"
#include "../Camera/CameraManager.h"
#include "../Mouse/Mouse.h"
#include "../Shader Manager/ShaderManager.h"
#include "Voxel.h"
#include "VoxelMerger.h"

class VoxelChunk
{
public:
	// Constructor for use with Density Data create world function (just a different way of reading the noise)
	VoxelChunk(std::shared_ptr<VoxelMerger> voxelMerger, int worldXPlacement, int worldYPlacement, int worldZPlacement, std::vector<std::vector<std::vector<float>>>& densityField);

	// Constructor for use with FBM Noise create world function
	VoxelChunk(std::shared_ptr<VoxelMerger> voxelMerger, int worldXPlacement, int worldYPlacement, int worldZPlacement, std::vector<std::vector<float>>& noiseValues);
	~VoxelChunk(){}
	void render(std::shared_ptr<CameraManager> cameraManager, std::shared_ptr<ShaderManager> shaderManager);
	glm::vec3 getWorldPlacement(){ return m_worldPlacement; }
	std::shared_ptr<BoundingBox> getBoundingBox(){ return m_chunkBbox; }
	static int getChunkSize(){ return m_chunkSize; }
	bool rayPickForDestruction(std::shared_ptr<VoxelMerger> voxelMerger, Ray mousePickRay, glm::vec3 &indexVal);
	void updateVisible(bool visible){ m_visible = visible; }

private:
	bool isCube(float densityField[8]); // helper function that basically just returns true if theres a cube or no cube at that point in the density data
	
	void removeVoxel(std::shared_ptr<VoxelMerger> voxelMerger, glm::vec3 indexVal);
	
	static int m_chunkSize;
	std::shared_ptr<Voxel> m_voxels[16][16][16]; // x, y, z list of voxels
	std::shared_ptr<BoundingBox> m_chunkBbox;
	bool m_visible;
	glm::vec3 m_worldPlacement; // origin/orientation point for the chunk
	GLuint m_meshVAO;
	GLuint m_chunkIndexCount;
};