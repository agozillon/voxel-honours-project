#pragma once

#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>
#include <vector>

class VoxelMerger{
public:
	VoxelMerger(){}
	~VoxelMerger(){}
	void createMesh(GLuint& vao, GLuint& indiceCount);
	void addVoxel(glm::vec3 position, float blockSize);
	

private:
	GLuint bindVBO(const GLvoid* data, GLint attribIndex, GLenum targetType, GLenum drawMode, GLenum dataType, GLboolean normalized, size_t sizeOfData, GLuint numberOfDataPerValue);
	void pushPolygon(glm::vec3 vert1, glm::vec3 vert2, glm::vec3 vert3, glm::vec2 uv1, glm::vec2 uv2, glm::vec2 uv3);
	std::vector<float> m_mergedMesh;
	std::vector<float> m_mergedUV;
};