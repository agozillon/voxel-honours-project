#include "VoxelChunkManager.h"
#include <iostream>


#pragma region "Constructors & Destructors"

/*
 Constructor for generating the terrain using density data 

@param CameraManager cameraManager - pointer to the camera manager the voxel renderer will use
@param ShaderManager shaderManager - pointer to the shader manager the voxel renderer will use
@param Texture2D voxelTexture - the single texture the voxels will use to render
@param vec3 worldSize - dimensionallity of data, data should be the same size along all axises for all data
						which means all X axis arrays should be the same size but it can be x16/y13/z10 as long
						as it stays the same for all X, Y and Z
@param Vector<Vector<Vector<float>>> densityField - list of density values to generate the world from

 */
VoxelChunkManager::VoxelChunkManager(std::shared_ptr<CameraManager> cameraManager, std::shared_ptr<ShaderManager> shaderManager, std::shared_ptr<Texture2D> voxelTexture, glm::vec3 worldSize,
	std::vector<std::vector<std::vector<float>>> densityField)
	: m_cameraManager(cameraManager), m_shaderManager(shaderManager), m_voxelTexture(voxelTexture)
{
	m_voxelMerger = std::shared_ptr<VoxelMerger>(new VoxelMerger());

	createWorldDensity(worldSize, densityField);
}

/*
Constructor for the voxel world that uses perlin noise based fractional brownian motion to generate it

kind of a 'deprecated' consturctor, along with the createWorld function, were used initially to generate the world
but now use the density data to generate it.

@param vec3 worldSize - height, width and length of the world in voxels in 1x1x1 cubes you wish the world to be
and a bunch of noise paremters and then procedurally generates the world. Should be a number divisable by 16(chunk size)

The Below are Generation Parameters: 
@param float frequency - increases the frequency of each octave of noise, gets increased by lacunarity after each octave of noise
frequency can be seen as the space between each noise hump, higher frequency more points.
@param float amplitude - increases the amplitude of each noise point for all octaves, increasing the regular value outputted
by the noise, it gets reduced each octave so noise(0.5) * amiplitude(0.25) = 0.25
@param float lacunarity - varies the frequency by multiplying this number against it after each octave has been added on
effectively making each level of noise more "zoomed out" or "zoomed in" or with more/less noise points
@param float persistence - varies the amplitude each octave same way as the frequency, essentially increases the noise troughs
in size or reduces depending on the value you put in

*/
VoxelChunkManager::VoxelChunkManager(std::shared_ptr<CameraManager> cameraManager, std::shared_ptr<ShaderManager> shaderManager, std::shared_ptr<Texture2D> voxelTexture,
	glm::vec3 worldSize, glm::vec3 frequency, float amplitude, float lacunarity, float persistence, int octaves)
	: m_cameraManager(cameraManager), m_shaderManager(shaderManager), m_voxelTexture(voxelTexture)
{
	// passed in world size divided by the chunk size to get the number of chunks we need
	m_numberOfChunks = glm::vec3(worldSize.x / VoxelChunk::getChunkSize(),
		worldSize.y / VoxelChunk::getChunkSize(), worldSize.z / VoxelChunk::getChunkSize());

	m_voxelMerger = std::shared_ptr<VoxelMerger>(new VoxelMerger());

	// instantiating the Perlin Noise generator and seeding it, saves recreating it each time we create a world
	m_perlinGenerator = std::unique_ptr<PerlinNoise>(new PerlinNoise(231.0f)); 

	// create the initial world via passed constructor values
	createWorldFBM(frequency, amplitude, lacunarity, persistence, octaves); 
}

#pragma endregion

/* 
	Function that generated the voxel world from density data so that it represents somewhat similar terrain to 
	the Dual Contouring Algorithm
*/
void VoxelChunkManager::createWorldDensity(glm::vec3 worldSize, std::vector<std::vector<std::vector<float>>> densityField)
{
	m_world.clear();

	m_numberOfChunks = worldSize / glm::vec3(VoxelChunk::getChunkSize(), VoxelChunk::getChunkSize(), VoxelChunk::getChunkSize());

	int startPoint = -worldSize.x;

	// Density data comes in points, rather than cubes one per corner 
	std::vector<std::vector<std::vector<float>>> densityChunks(17, std::vector<std::vector<float>>(17, std::vector<float>(17)));
	
	for (int chunkI = 0; chunkI < m_numberOfChunks.x; ++chunkI)
	{
		for (int chunkJ = 0; chunkJ < m_numberOfChunks.y; ++chunkJ)
		{
			for (int chunkK = 0; chunkK < m_numberOfChunks.z; ++chunkK)
			{
				// + 1 for density data as it deal with corner points (and there is always one more point in a row than the number of voxels)
				for (int i = 0; i < VoxelChunk::getChunkSize() + 1; ++i)
				{
					for (int j = 0; j < VoxelChunk::getChunkSize() + 1; ++j)
					{
						for (int k = 0; k < VoxelChunk::getChunkSize() + 1; ++k)
						{

							densityChunks[i][j][k] = densityField[i + (chunkI * 16)][j + (chunkJ * 16)][k + (chunkK * 16)];

							if (i == 16 && j == 16 && k == 16)
							{
								m_world.push_back(std::shared_ptr<VoxelChunk>(new VoxelChunk(m_voxelMerger, startPoint + (chunkI * (VoxelChunk::getChunkSize() * 2)), startPoint + 
									(chunkJ * (VoxelChunk::getChunkSize() * 2)), startPoint + (chunkK * (VoxelChunk::getChunkSize() * 2)), densityChunks)));
								densityChunks.empty();
							}
						}
					}
				}
			}
		}
	}
	
}


/*
	Simple function that creates a procedural world (of the size specified in the constructor) 
	from a Perlin Noise Generated Height Map, can be difficult to get good/nice looking terrain
*/
void VoxelChunkManager::createWorldFBM(glm::vec3 frequency, float amplitude, float lacunarity, float persistence, int octaves)
{
	// Simple Perlin Heightmap Generation
	// For the number of grid values on the x and y
	int i, j, k;
	float width = m_numberOfChunks.x * VoxelChunk::getChunkSize();
	float height = m_numberOfChunks.y * VoxelChunk::getChunkSize();
	float length = m_numberOfChunks.z * VoxelChunk::getChunkSize();

	std::vector<std::vector<float>> noiseValues;

	for (i = 0; i < width; ++i)
	{
		std::vector<float> tmp;
		noiseValues.push_back(tmp);
		for (j = 0; j < length; ++j)
		{
			float noise = std::floor((height - 1) * m_perlinGenerator->fractionalBrownianMotion(glm::vec3(i / width, 0.8, j / length), frequency, amplitude, lacunarity, persistence, octaves, true));
		
			if (noise >= height)
				noise = height - 1;
			
			noiseValues[i].push_back(noise);
		}
	}

	m_world.clear(); // clear the old world
	
	for (i = 0; i < m_numberOfChunks.x; ++i) // x
	{
		for (j = 0; j < m_numberOfChunks.y; ++j) // y
		{
			for (k = 0; k < m_numberOfChunks.z; ++k) // z
			{
				// Basically we want to start from the top set of voxels if there is more than one
				// and not the bottom
				int y = 0;
				if (m_numberOfChunks.y > 1)
					y = m_numberOfChunks.y - (j + 1);
				else
					y = j;

				
				m_world.push_back(std::shared_ptr<VoxelChunk>(new VoxelChunk(m_voxelMerger, i, y, k, noiseValues)));
			}
		}
	}

}

/*
   Function that renders the Voxel World by calling all the voxel chunks render functions.
*/
void VoxelChunkManager::renderWorld()
{
	// Sets the shader to Textured Shader (doesn't support other shader types at the moment)
	// not hugelly difficult to extend to.
	m_shaderManager->getShaderProgram("Textured Shader")->use();

	// bind texture
	m_voxelTexture->bindTexture(GL_TEXTURE0);

	// loops through all the voxel chunks and calls there render function passing in the camera manager
	// and shader manager
	int currentChunk = 0;
	while (currentChunk < m_world.size())
	{
		m_world[currentChunk]->render(m_cameraManager, m_shaderManager);

		currentChunk += 1;
	}
}

void VoxelChunkManager::renderDebugBoxes(GLuint meshID, GLuint vertexCount)
{
	for (int i = 0; i < m_world.size(); ++i)
		m_world[i]->getBoundingBox()->draw(m_shaderManager, m_cameraManager, meshID, vertexCount);
}

bool VoxelChunkManager::rayPickForDestruction(Ray mousePickRay, glm::vec3& indexVal)
{
	int savedI = 0;
	glm::vec3 collisionPoint;
	glm::vec3 nearestPoint(0, 0, 0);
	bool collision = false;

	for (int i = 0; i < m_world.size(); ++i)
	{
		if (m_world[i]->getBoundingBox()->detectRayCollision(mousePickRay.rayOrigin, mousePickRay.rayDirection, collisionPoint))
		{
		//	std::cout << "Collision Point: " << collisionPoint.x << " " << collisionPoint.y << " " << collisionPoint.z << std::endl;
		//	std::cout << "Mouse Pick Ray: " << mousePickRay.rayOrigin.x << " " << mousePickRay.rayOrigin.y << " " << mousePickRay.rayOrigin.z << std::endl;
		//	std::cout << " -ve " << mousePickRay.rayOrigin.x - collisionPoint.x << " " << mousePickRay.rayOrigin.y - collisionPoint.y << " " << mousePickRay.rayOrigin.z - collisionPoint.z << std::endl;

			if (abs(nearestPoint.x - mousePickRay.rayOrigin.x) >= abs(collisionPoint.x - mousePickRay.rayOrigin.x)
				&& abs(nearestPoint.y - mousePickRay.rayOrigin.y) >= abs(collisionPoint.y - mousePickRay.rayOrigin.y)
				&& abs(nearestPoint.z - mousePickRay.rayOrigin.z) >= abs(collisionPoint.z - mousePickRay.rayOrigin.z)
				|| nearestPoint.x == 0 && nearestPoint.y == 0 && nearestPoint.z == 0)
			{
				nearestPoint = collisionPoint;
				savedI = i;
			}
		}

	}
	
	if (savedI < m_world.size())
	{
		glm::vec3 voxelIndex;
		collision = m_world[savedI]->rayPickForDestruction(m_voxelMerger, mousePickRay, voxelIndex);
	
		voxelIndex = voxelIndex + glm::vec3(std::ceil(m_world[savedI]->getWorldPlacement().x / 2) + (m_numberOfChunks.x / 2) * VoxelChunk::getChunkSize(),
			std::ceil(m_world[savedI]->getWorldPlacement().y / 2) + (m_numberOfChunks.y / 2) * VoxelChunk::getChunkSize(),
			std::ceil(m_world[savedI]->getWorldPlacement().z / 2) + (m_numberOfChunks.z / 2) * VoxelChunk::getChunkSize());
	
		std::cout << voxelIndex.x << " " << voxelIndex.y << " " << voxelIndex.z << std::endl;

		indexVal = voxelIndex;
	}

	return collision;
}
