#include "VoxelChunk.h"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>


// NOTE TO SELF: Probably a good idea to change this so that the cube we render is offset by 
// a model matrix instead of just being offset by changing all the vertex positions of a box
// Also preferably it would position everything from the middle rather than position it from the bottom left
// not as big a deal as the first one, but might be easier.

int VoxelChunk::m_chunkSize = 16;

// Can change this function to make the view of the underlying density data  different
// the way it currently is it shows anything that the surface runs through even slightly
// (exits if its less than 0 or == 0 with true making a surface (0) or under surface cube(< 0 visible)
// you can do it and only get ones under the surface and not on the surface with density data > 0
// return false. Theres a couple of other variations possible. But the one needed for deformation is
// less than 0 or == 0
bool VoxelChunk::isCube(float densityField[8])
{
	for (int i = 0; i < 8; ++i)
	{
		if (densityField[i] <= 0)
			return true;
	}

	return false;
}

// Voxel Chunk constructor used for generation with density data
VoxelChunk::VoxelChunk(std::shared_ptr<VoxelMerger> voxelMerger, int worldXPlacement, int worldYPlacement, int worldZPlacement, std::vector<std::vector<std::vector<float>>>& densityField)
{
	m_visible = true;
	float cubeCorners[8];
	m_worldPlacement = glm::vec3(worldXPlacement, worldYPlacement, worldZPlacement);

	glm::vec3 voxelPos;

	// if any part of the density data is -ve or 0 then its part of the surface, if its positive
	// its not and shouldn't be a visible voxel
	for (int i = 0; i < m_chunkSize; ++i)
	{
		for (int j = 0; j < m_chunkSize; ++j)
		{
			for (int k = 0; k < m_chunkSize; ++k)
			{
				cubeCorners[0] = densityField[i][j][k]; // bottom left
				cubeCorners[1] = densityField[i][j + 1][k]; // top left
				cubeCorners[2] = densityField[i][j][k + 1]; // back bottom left
				cubeCorners[3] = densityField[i][j + 1][k + 1]; // back top left
				cubeCorners[4] = densityField[i + 1][j][k]; // bottom right
				cubeCorners[5] = densityField[i + 1][j + 1][k]; // top right
				cubeCorners[6] = densityField[i + 1][j][k + 1]; // back bottom right
				cubeCorners[7] = densityField[i + 1][j + 1][k + 1]; // back top right 

				voxelPos.x = worldXPlacement + (i * 2) + 1;
				voxelPos.y = worldYPlacement + (j * 2) + 1;
				voxelPos.z = worldZPlacement + (k * 2) + 1;

				if (isCube(cubeCorners))
				{
					m_voxels[i][j][k] = std::shared_ptr<Voxel>(new Voxel(true, voxelPos, glm::vec3(1, 1, 1)));

					voxelMerger->addVoxel(voxelPos, 1);
				}
				else
				{
					m_voxels[i][j][k] = std::shared_ptr<Voxel>(new Voxel(false, voxelPos, glm::vec3(1, 1, 1)));
				}
			}
		}
	}

	// we base the chunk from the bottom left most position. the * 2 should be the voxel size, for speed of programming
	// at the moment its just a hardcoded 2 
	float halfChunk = (m_chunkSize * 2) / 2;
	m_chunkBbox = std::shared_ptr<BoundingBox>(new BoundingBox(glm::vec3(m_worldPlacement.x + halfChunk, m_worldPlacement.y + halfChunk, m_worldPlacement.z + halfChunk), 
		glm::vec3(halfChunk, halfChunk, halfChunk)));

	// Creates the mesh out of all the voxels we've been adding to the voxelMerger via the add Voxel function
	voxelMerger->createMesh(m_meshVAO, m_chunkIndexCount);

}


// Voxel chunk constructor used for generation with perlin noise based FBM (treats noise like a heightmap)
VoxelChunk::VoxelChunk(std::shared_ptr<VoxelMerger> voxelMerger, int worldXPlacement, int worldYPlacement, int worldZPlacement, std::vector<std::vector<float>>& noiseValues)
{
	for (int i = 0; i < m_chunkSize; ++i)
	{
		for (int j = m_chunkSize; j > 0; --j)
		{
			for (int k = 0; k < m_chunkSize; ++k)
			{	
				if (noiseValues[(worldXPlacement * m_chunkSize) + i][(worldZPlacement * m_chunkSize) + k] > 0)
				{
					m_voxels[i][j-1][k] = std::shared_ptr<Voxel>(new Voxel(false));
					noiseValues[(worldXPlacement * m_chunkSize) + i][(worldZPlacement * m_chunkSize) + k] -= 1;
				}
				else
				{
					m_voxels[i][j-1][k] = std::shared_ptr<Voxel>(new Voxel(true));
				}

				// Add voxel to the chunk mesh, we don't render single Voxel Cubes as that
				// clogs up the GPU pipeline and means 4890(size of a 16x16x16 chunk) voxels
				// runs at 2 FPS. 
				if (m_voxels[i][j-1][k]->getVisible() == true)
				{
					voxelMerger->addVoxel(glm::vec3(((worldXPlacement * m_chunkSize) * 2) + (i * 2),
						((worldYPlacement * m_chunkSize) * 2) + ((j-1) * 2), ((-worldZPlacement * m_chunkSize) * 2) + (-k * 2)), 1);
				}
			}
		}
	}

	// Creates the mesh out of all the voxels we've been adding to the voxelMerger via the add Voxel function
	voxelMerger->createMesh(m_meshVAO, m_chunkIndexCount);
}

/*
	Render this Voxel Chunk
	@Param Shared Pointer ShaderManager shaderManager - pointer to the currently active shader manager
	@Param Shared Pointer CameraManager cameraManager - pointer to the currently active camera manager 
*/
void VoxelChunk::render(std::shared_ptr<CameraManager> cameraManager, std::shared_ptr<ShaderManager> shaderManager)
{
	if (m_visible == true)
	{
		// model * view 
		glm::mat4 mv = cameraManager->getView() * mat4(1.0);

		shaderManager->getShaderProgram("Textured Shader")->setUniformMatrix4fv("modelview", 1, false, glm::value_ptr(mv));
		shaderManager->getShaderProgram("Textured Shader")->setUniformMatrix4fv("projection", 1, false, glm::value_ptr(cameraManager->getProjection()));

		glBindVertexArray(m_meshVAO);
		glDrawArrays(GL_TRIANGLES, 0, m_chunkIndexCount);
	}
}

void VoxelChunk::removeVoxel(std::shared_ptr<VoxelMerger> voxelMerger, glm::vec3 indexVal)
{
	glm::vec3 voxelPos;

	// if any part of the density data is -ve or 0 then its part of the surface, if its positive
	// its not and shouldn't be a visible voxel
	for (int i = 0; i < m_chunkSize; ++i)
	{
		for (int j = 0; j < m_chunkSize; ++j)
		{
			for (int k = 0; k < m_chunkSize; ++k)
			{
				voxelPos.x = m_worldPlacement.x + (i * 2) + 1;
				voxelPos.y = m_worldPlacement.y + (j * 2) + 1;
				voxelPos.z = m_worldPlacement.z + (k * 2) + 1;

				if (m_voxels[i][j][k]->getVisible())
				{
					m_voxels[i][j][k] = std::shared_ptr<Voxel>(new Voxel(true, voxelPos, glm::vec3(1, 1, 1)));

					voxelMerger->addVoxel(voxelPos, 1);
				}
			}
		}
	}

	// Creates the mesh out of all the voxels we've been adding to the voxelMerger via the add Voxel function
	voxelMerger->createMesh(m_meshVAO, m_chunkIndexCount);
}

bool VoxelChunk::rayPickForDestruction(std::shared_ptr<VoxelMerger> voxelMerger, Ray mousePickRay, glm::vec3 &indexVal)
{
	int i = 0, j = 0, k = 0, savedI = 0, savedJ = 0, savedK = 0;
	bool collision = false;

	glm::vec3 collisionPoint;
	glm::vec3 nearestPoint(0,0,0); 
	while (i < m_chunkSize)
	{
		while (j < m_chunkSize)
		{
			while (k < m_chunkSize)
			{
				
				if (m_voxels[i][j][k]->getVisible() &&
					m_voxels[i][j][k]->getCollisionBox()->detectRayCollision(mousePickRay.rayOrigin,
					mousePickRay.rayDirection, collisionPoint))
				{
					 
					if (abs(nearestPoint.x - mousePickRay.rayOrigin.x) >= abs(collisionPoint.x - mousePickRay.rayOrigin.x)
						&& abs(nearestPoint.y - mousePickRay.rayOrigin.y) >= abs(collisionPoint.y - mousePickRay.rayOrigin.y)
						&& abs(nearestPoint.z - mousePickRay.rayOrigin.z) >= abs(collisionPoint.z - mousePickRay.rayOrigin.z)
						|| nearestPoint.x == 0 && nearestPoint.y == 0 && nearestPoint.z == 0)
					{
						nearestPoint = collisionPoint;
						savedI = i; savedJ = j; savedK = k;
						collision = true;
					}
				}
				++k;
			}
			++j;
			k = 0;
		}
		++i;
		k = 0; j = 0;
	}

	if (collision == true)
	{
		m_voxels[savedI][savedJ][savedK]->updateVisible(false);
		removeVoxel(voxelMerger, glm::vec3(savedI, savedJ, savedK));
		indexVal = glm::vec3(savedI, savedJ, savedK);
	}

	return collision;
}