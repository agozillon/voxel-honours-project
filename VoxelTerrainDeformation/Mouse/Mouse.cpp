#include "Mouse.h"
#include "../App Control/AppControl.h"


#include <iostream>
#include <math.h>

Mouse * Mouse::m_instance = nullptr;
Mouse::Mouse(){ }

void Mouse::createInstance()
{
	if( m_instance ) return;
	m_instance = new Mouse();
}

Mouse * Mouse::getInstance()
{
	if( !m_instance ) createInstance();
	return m_instance;
}

void Mouse::deleteInstance()
{
	if( !m_instance ) return;
	delete m_instance;
	m_instance = NULL;
}

void Mouse::updateEvent(const SDL_Event e)
{
	m_currentEventState = e;

	m_timeStamp = e.button.timestamp;
	
	m_x = e.motion.x;
	m_y = AppControl::getInstance()->getWindowSize().y - e.motion.y;

	if( e.button.button == SDL_BUTTON_LEFT && e.button.type == SDL_MOUSEBUTTONDOWN )
		m_left = BUTTON_STATE_DOWN;
	 else if(e.button.button == SDL_BUTTON_LEFT && e.button.type == SDL_MOUSEBUTTONUP )
		m_left = BUTTON_STATE_UP;
	

	if( e.button.button == SDL_BUTTON_RIGHT && e.button.type == SDL_MOUSEBUTTONDOWN )
		m_right = BUTTON_STATE_DOWN;
	 else if(e.button.button == SDL_BUTTON_RIGHT && e.button.type == SDL_MOUSEBUTTONUP )
		m_right = BUTTON_STATE_UP;
	

	if( e.button.button == SDL_BUTTON_MIDDLE && e.button.type == SDL_MOUSEBUTTONDOWN )
		m_middle = BUTTON_STATE_DOWN;
	else if(e.button.button == SDL_BUTTON_MIDDLE && e.button.type == SDL_MOUSEBUTTONUP )
		m_middle = BUTTON_STATE_UP;

	// re-allgining the mouses x 0 - width and y height - 0 co-ordinates to screen space co-ordinates of 
	// -1 to 1, so window space to NDC space
	m_NDCX = (m_currentEventState.motion.x / (AppControl::getInstance()->getWindowSize().x / 2.0) - 1.0f); //
	m_NDCY = (1.0f - m_currentEventState.motion.y / (AppControl::getInstance()->getWindowSize().y / 2.0));
}



// kinda needs to be edited to be more maleable, as changing the perspective projection will break it right now
Ray Mouse::calculateRay(std::shared_ptr<CameraManager> cameraManager)
{
	// calculating the view ratio of the cameras projection this will change the position of the 
	//NDC space point on sreen/ un-clip it into camera space when we multiply it later on!
	double viewRatio = std::tan(((float) 3.14159265359 / (180.0f/cameraManager->getFOV()) / 2.00f)); 
	
	// camera space X and Y, multiply the aspect ratio to get the proper screen ratio, and then
	// multiply by the cameras view ratio to get the proper camera space x position (i.e pin hole camera projection)
	double clipSpaceX = m_NDCX * AppControl::getInstance()->getAspectRatio() * viewRatio;
	double clipSpaceY = m_NDCY * viewRatio;
	 
	// we're getting the camera near and far from the current screen space positions ratio, as the distance increases
	// from the camera the position will change linearly into the world based off of the cameras FOV I.E  \_/
	// lines/rays would go out diagonally into the world but be percieved as straight lines to the viewer
	// so finally getting the camera space positions 
	glm::vec4 cameraNear = glm::vec4((float)(clipSpaceX * cameraManager->getNear()), (float)(clipSpaceY * cameraManager->getNear()), -cameraManager->getNear(), 1);
	glm::vec4 cameraFar = glm::vec4((float)(clipSpaceX * cameraManager->getFar()), (float)(clipSpaceY * cameraManager->getFar()), -cameraManager->getFar(), 1);

	// now from camera space back to world space using the inverse camera matrix to un-translate, rotate
	// and scale the positions back 
	glm::mat4 inverseView = glm::inverse(cameraManager->getView());
	glm::vec4 worldSpaceNear = inverseView * cameraNear;
	glm::vec4 worldSpaceFar =  inverseView * cameraFar;

	Ray tmpRay;

	// the origin is now the world space near as we wish to start at the cameras initial point
	tmpRay.rayOrigin = glm::vec3(worldSpaceNear.x, worldSpaceNear.y, worldSpaceNear.z);
	// and the direction is from this origin point to the far plane world space point!
	tmpRay.rayDirection = glm::vec3(worldSpaceFar.x - worldSpaceNear.x, worldSpaceFar.y - worldSpaceNear.y, worldSpaceFar.z - worldSpaceNear.z);
	// and we now normalize it as we do not need the full length distance just the direction
	tmpRay.rayDirection = glm::normalize(tmpRay.rayDirection);

	return tmpRay;
}
