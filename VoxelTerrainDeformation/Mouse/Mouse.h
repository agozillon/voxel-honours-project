#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <memory>
#include <SDL.h>

#include "../Camera/CameraManager.h"

enum MouseButtonState
{
	BUTTON_STATE_DOWN,
	BUTTON_STATE_UP
};

struct Ray
{
	glm::vec3 rayOrigin;
	glm::vec3 rayDirection;
};

// Mouse class that handles Ray casting into 3D space and 2D co-ordinate checking and clicking
// This was used previously in the 3rd year group project, The Ray casting is my code and the 
// mouse button state stuff is Stephen Mcgroarty's. 
class Mouse
{

public:
	// standard Singleton function and setup
	static void createInstance();  
	static void deleteInstance();
	static Mouse * getInstance();

	// function for calculating the ray, saves results into rayOrigin and rayDirection
	Ray calculateRay(std::shared_ptr<CameraManager> cameraManager);

	void updateEvent(const SDL_Event e);
	const inline SDL_Event & getEvent() const { return m_currentEventState; }

	const inline MouseButtonState getRightState() const { return m_right; }
	const inline MouseButtonState getLeftState()  const { return m_left;  }
	const inline MouseButtonState getMiddleState() const { return m_middle;}

	const inline int getTimeStamp() const { return m_timeStamp; }
	const inline int getX() const { return m_x; }
	const inline int getY() const { return m_y; }
	
	const inline double getNDCX() const { return m_NDCX; }
	const inline double getNDCY() const { return m_NDCY; }

private:
	// holds the currentEventState so the class can access, variables from it and check it
	// if required
	SDL_Event m_currentEventState;
	int m_x, m_y;
	double m_NDCX, m_NDCY;
	unsigned int m_timeStamp;
	
	MouseButtonState m_right, m_left, m_middle;
	
	// private constructor/destructor as singletons do, static mouse instance to link to itself once
	// instantiated
	static Mouse * m_instance;
	Mouse();
	~Mouse(){}
};

