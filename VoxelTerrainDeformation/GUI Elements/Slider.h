#pragma once
#include <memory>
#include <string>

#include "../Collision Detection/BoundingSquare.h"
#include "../Camera/CameraManager.h"
#include "../Shader Manager/ShaderManager.h"
#include "../Texture/Texture2D.h"
#include "../Texture/TextToTexture.h"

class Slider
{
public:
	Slider(std::shared_ptr<TextToTexture> textToTexture, std::string name, std::shared_ptr<Texture2D> bgTexture, std::shared_ptr<Texture2D> sliderTexture, glm::vec2 pos, glm::vec2 bgScale, glm::vec2 sliderScale, float min, float max, std::string shader, GLuint meshVAO, GLuint meshIndexCount);
	~Slider(){}
	void render(std::shared_ptr<ShaderManager> shaderManager, std::shared_ptr<CameraManager> cameraManager);
	void moveSlider(float xPos);
	float getSliderValue(){ return m_sliderValue; }
	void updateMin(float min){ m_min = min; }
	void updateMax(float max){ m_max = max; }
	float getMin(){ return m_min; }
	float getMax(){ return m_max; }
	float getCurrentPos(){ return m_currentPos; }
	std::shared_ptr<BoundingSquare> getBoundingSquare(){ return m_boundingSquare; }

private:
	float calculateSliderValue();
	float m_min, m_max, m_currentPos, m_sliderValue;
	glm::vec2 m_bgScale, m_sliderScale;
	glm::vec2 m_pos;
	std::string m_shader, m_name;
	std::shared_ptr<Texture2D> m_textTexture, m_bgTexture, m_sliderTexture;
	std::shared_ptr<BoundingSquare> m_boundingSquare;
	std::shared_ptr<TextToTexture> m_textToTexture;

	GLuint m_meshVAO, m_meshIndexCount;
};