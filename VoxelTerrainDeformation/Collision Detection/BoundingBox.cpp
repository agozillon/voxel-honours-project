#include "BoundingBox.h"

#include <iostream>
#include <stack>
#include <vector>
#include <sstream>


// basic constructor that accepts the BoundingBox's position and dimensions and sets them up, USED IN RT3D of Andrew Gozillon
BoundingBox::BoundingBox(const glm::vec3 bboxPositions, const glm::vec3 bboxDimensions)
{
	dimensions.x = bboxDimensions.x;
	dimensions.y = bboxDimensions.y;
	dimensions.z = bboxDimensions.z;

	position.x = bboxPositions.x;
	position.y = bboxPositions.y;
	position.z = bboxPositions.z;
}

// function primarily for testing it draws the BoundingBox on screen 
void BoundingBox::draw(std::shared_ptr<ShaderManager> shaderManager, std::shared_ptr<CameraManager> cameraManager, GLuint meshID, GLuint vertexCount)
{
	std::stack<mat4> mStack;
	mStack.push(mat4(1.0));
	mStack.top() = glm::translate(mStack.top(), position);
	mStack.top() = glm::scale(mStack.top(), dimensions);

	mat4 temp = cameraManager->getView() * mStack.top();

	shaderManager->getShaderProgram("Textured Shader")->setUniformMatrix4fv("modelview", 1, false, glm::value_ptr(temp));
	shaderManager->getShaderProgram("Textured Shader")->setUniformMatrix4fv("projection", 1, false, glm::value_ptr(cameraManager->getProjection()));

	glBindVertexArray(meshID);
	glDrawArrays(GL_TRIANGLES, 0, vertexCount);
}

// function that accepts a pointer to a BoundingBox and checks for a collision between the current bounding box
// and the passed in BoundingBox and then returns either true or false depending on if they've colided
// USED IN RT3D of Andrew Gozillon
const bool BoundingBox::detectBoxCollision(BoundingBox * colideeBoundingBox)
{
	// really simple Axis alligned bounding box detection checks if a collision is actually possible for each side
	// and if its not then a collision can't be happening so it exits early with a false value if it gets through them all
	// correctly then it returns true
	if (position.x + dimensions.x < colideeBoundingBox->getPosition().x - colideeBoundingBox->getDimensions().x)
		return false;

	if (position.x - dimensions.x > colideeBoundingBox->getPosition().x + colideeBoundingBox->getDimensions().x)
		return false;

	if (position.y + dimensions.y < colideeBoundingBox->getPosition().y - colideeBoundingBox->getDimensions().y)
		return false;

	if (position.y - dimensions.y > colideeBoundingBox->getPosition().y + colideeBoundingBox->getDimensions().y)
		return false;

	if (position.z + dimensions.z < colideeBoundingBox->getPosition().z - colideeBoundingBox->getDimensions().z)
		return false;

	if (position.z - dimensions.z > colideeBoundingBox->getPosition().z + colideeBoundingBox->getDimensions().z)
		return false;

	// returns the bool 
	return true;
}

// simple function that calculates the distance needed to seperate to bounding boxes if they've entered each other
// during intersection, can happen during the interpolation phase of the traversingPath function and can cause 
// AI units to permanently stick to an obstacle (rarely if at all happens to player controlled units).
const vec3 BoundingBox::seperationDistance(BoundingBox * collider)  
{
	glm::vec3 seperationDist;
	// if the colliders position is greater than position x on the bounding box then its on the "right" side of the
	// collision box so we need to use the right side of the colidee and the left side of the colliders box and then negate the distance
	// to calculate the distance requried to seperate them. The 1.1f is just to increase the seperation distance a little so its not right on the edge.
	if(collider->getPosition().x > position.x)
	{
		seperationDist.x = ((position.x + dimensions.x) - (collider->getPosition().x - collider->getDimensions().x)) * 1.1f;
	}
	else 
	{
		seperationDist.x = ((position.x - dimensions.x) - (collider->getPosition().x + collider->getDimensions().x)) * 1.1f;
	}

	// same as above except for with the Y
	if(collider->getPosition().y + collider->getDimensions().y > position.y)
	{
		seperationDist.y = ((position.y + dimensions.y) - (collider->getPosition().y - collider->getDimensions().y)) * 1.1f;
	}
	else
	{
		seperationDist.y = ((position.y - dimensions.y) - (collider->getPosition().y + collider->getDimensions().y)) * 1.1f;
	}

	// same as above except for with the Z
	if(collider->getPosition().z + collider->getDimensions().z > position.z)
	{
		seperationDist.z = ((position.z + dimensions.z) - (collider->getPosition().z - collider->getDimensions().z)) * 1.1f;
	}
	else
	{
		seperationDist.z = ((position.z - dimensions.z) - (collider->getPosition().z + collider->getDimensions().z)) * 1.1f;
	}

	return seperationDist;
}

// 3d ray collision check, had a lot of help from online sources(since my maths isn't great) however it
// calculates where a ray line intersects the 6 planes of the bounding box (represented as a line)
const bool BoundingBox::detectRayCollision(const glm::vec3 rayPoint, const glm::vec3 rayDirection, glm::vec3 &collisionPoint)
{
	bool collision = true;
	
	glm::vec3 max, min;
	glm::vec3 bMax, bMin;


	// calculating min/max of bboxs x, y and z dimensions each of these max and min values
	// creates our bounding box planes for that specific dimension
	bMax.x = position.x + dimensions.x; bMin.x = position.x - dimensions.x;
	bMax.y = position.y + dimensions.y;	bMin.y = position.y - dimensions.y;
	bMax.z = position.z + dimensions.z; bMin.z = position.z - dimensions.z;
	
	// calculating where on the Bounding Boxs X plane the ray interects
	// the min 
	if(rayDirection.x >= 0)
	{
		// solving for 2 lines bMax is one rayPoint is one, we get the direction
		// from the rays origin to the bounding boxs max x and then divide it by
		// the rays direction to see where it crosses that plane at a -ve value
		// means it's behind the rays origin so the ray intersected it some point "in the past"
		// another way of looking at it (my prefferred way for visualizing it at least so may not be
		// accurate) is that we're getting the number of increments of the rays direction
		// to get to the bounding boxes minimum and maximum ranges I.E ray direction x at 0.5
		// box x min range = 2, 2 / 0.5 = 4 thus it takes 4 increments of the ray to get here
		max.x = (bMax.x - rayPoint.x) / rayDirection.x;
		min.x = (bMin.x - rayPoint.x) / rayDirection.x;
	}
	else
	{
		min.x = (bMax.x - rayPoint.x) / rayDirection.x;
		max.x = (bMin.x - rayPoint.x) / rayDirection.x;
	}

	if(rayDirection.y >= 0)
	{
		max.y = (bMax.y - rayPoint.y) / rayDirection.y;
		min.y = (bMin.y - rayPoint.y) / rayDirection.y;
	}
	else
	{
		min.y = (bMax.y - rayPoint.y) / rayDirection.y;
		max.y = (bMin.y - rayPoint.y) / rayDirection.y;
	}
		
	// basically if the rays min.x intersect point is greater than
	// the rays max y intersect point or min.y is greater than max.x 
	// then it doesn't intersect as the ray is travelling between two planes
	// that never allow for a connection within the object I.E overshoots
	// min x should be lower than max y and min y should be lower than max x 
	// for a ray to travel between them!
	// My visualized (and perhaps inaccurate) way of looking at it is that 
	// if the minimum x's increments are greater than the maximum y's increments
	// then the ray overshoots / goes above our box e.g min x increments = 10 to get
	// to the desired plane/location and max y increments = 8 to get to its desired plane
	// box min x = 1 box max y = 2, ray x = 0.10 ray y = 0.25, 10 * 0.10 = 1 y with 10 increments
	// = 2.50! so when we just reach the minimum x we've overshot the maximum y by 2 increments!
	if(min.x > max.y || min.y > max.x)
		return false;

	// these have no bearing what so ever on the check
	// it's essentially our min and max scale value to scale the ray direction with
	// to help get the intersection point. we want the smallest number and largest of the increments
	//to increment our ray to get the very first touch of the cube instead of too far in etc
	if(min.y > min.x)
		min.x = min.y;

	if(max.y < max.x)
		max.x = max.y;

	// calculating a z plane using the min and max, checks if the ray
	// is greater or smaller as this will change which is the min and which is the max
	// of the bounding box in relation to the rays direction
	if(rayDirection.z >= 0)
	{
		max.z = (bMax.z - rayPoint.z) / rayDirection.z;
		min.z = (bMin.z - rayPoint.z) / rayDirection.z;
	}
	else
	{
		min.z = (bMax.z - rayPoint.z) / rayDirection.z;
		max.z = (bMin.z - rayPoint.z) / rayDirection.z;
	}

	if(min.x > max.z || min.z > max.x)
		return false;

	if(min.z > min.x)
		min.x = min.z;

	if(max.z < max.x)
		max.x = max.z;

	// rays origin point + the number of ray increments to the intersect point
	// multiplied by our rays direction in all cases
	collisionPoint =  rayPoint + (min.x * rayDirection);
	
	return collision;
}


