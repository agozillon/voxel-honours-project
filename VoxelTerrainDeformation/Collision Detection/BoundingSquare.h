#pragma once

#include <glm/gtc/type_ptr.hpp>

class BoundingSquare{
public:
	BoundingSquare(glm::vec2 pos, glm::vec2 dimension){ m_pos = pos; m_dimension = dimension; }
	~BoundingSquare(){}
	glm::vec2 getPos(){ return m_pos; }
	glm::vec2 getDimension(){ return m_dimension; }
	void updatePos(glm::vec2 pos){ m_pos = pos; }
	void updateDimension(glm::vec2 dimension){ m_dimension = dimension; }
	bool detectPointCollision(glm::vec2 collisionPos);

private:
	glm::vec2 m_pos, m_dimension;
};