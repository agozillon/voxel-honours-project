#include "DemoState.h"
#include "../Mouse/Mouse.h"
#include "../Texture/TextToTexture.h"
#include "../Voxel Renderer/Voxel.h"

#include <cmath>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <fstream>
#include <SDL_keyboard.h>
#include <windows.h> 


#pragma region Constructors & Destructors 

// constructor
DemoState::DemoState()
{
	// initalize all relevant managers
	m_cameraManager = std::shared_ptr<CameraManager>(new CameraManager());
	m_shaderManager = std::shared_ptr<ShaderManager>(new ShaderManager());
	m_textureLoader = std::shared_ptr<TextureLoader>(new TextureLoader());
	m_assimpLoader = std::shared_ptr<AssimpLoader>(new AssimpLoader());
	m_perlinNoise = std::shared_ptr<PerlinNoise>(new PerlinNoise(1637));
	m_cameraManager->move(0, 15, 0);
}

// destructor
DemoState::~DemoState()
{
}

#pragma endregion 

// function that is used to initilize all of the States variables and objects
void DemoState::init()
{
	renderWorld = 0;
	mouseLastPressedTimer = 0;

	// load cube
	m_assimpLoader->load("Resources/Models/cube.obj", m_cubeVAO, m_cubeIndexCount);
	
	GLuint tmpVAO;
	GLint tmpIndexCount;

	m_assimpLoader->load("Resources/Models/quad.obj", tmpVAO, tmpIndexCount);

	// load texture
	m_voxelTexture = m_textureLoader->loadTexture("Resources/Textures/cube.jpg");
	m_terrainTexture = m_textureLoader->loadTexture("Resources/Textures/grass.bmp");
	m_sliderTexture = m_textureLoader->loadTexture("Resources/Textures/slider.jpg");
	m_sliderBGTexture = m_textureLoader->loadTexture("Resources/Textures/sliderbg.png");

	// create TextToTexture object that we'll use to create the slider text
	std::shared_ptr<TextToTexture> sliderTextCreator = std::shared_ptr<TextToTexture>(new TextToTexture("Resources/Fonts/MavenPro-Regular.ttf", 10));
	
	// initialize our noise sliders
	numberOfSliders = 7;
	// 3D Frequency X Slider
	m_slider[0] = std::shared_ptr<Slider>(new Slider(sliderTextCreator, "Frequency X: ", m_sliderBGTexture, m_sliderTexture, glm::vec2(-0.75f, 0.925f), glm::vec2(0.2, 0.025), glm::vec2(0.05, 0.025), -10, 10, "Textured Shader", tmpVAO, tmpIndexCount));
	// 3D Frequency Y Slider
	m_slider[1] = std::shared_ptr<Slider>(new Slider(sliderTextCreator, "Frequency Y: ", m_sliderBGTexture, m_sliderTexture, glm::vec2(-0.75f, 0.825f), glm::vec2(0.2, 0.025), glm::vec2(0.05, 0.025), -10, 10, "Textured Shader", tmpVAO, tmpIndexCount));
	// 3D Frequency Z Slider
	m_slider[2] = std::shared_ptr<Slider>(new Slider(sliderTextCreator, "Frequency Z: ", m_sliderBGTexture, m_sliderTexture, glm::vec2(-0.75f, 0.725f), glm::vec2(0.2, 0.025), glm::vec2(0.05, 0.025), -10, 10, "Textured Shader", tmpVAO, tmpIndexCount));
	// Amplitude Slider 
	m_slider[3] = std::shared_ptr<Slider>(new Slider(sliderTextCreator, "Amplitude: ", m_sliderBGTexture, m_sliderTexture, glm::vec2(-0.75f, 0.625f), glm::vec2(0.2, 0.025), glm::vec2(0.05, 0.025), -10, 10, "Textured Shader", tmpVAO, tmpIndexCount));
	// Lacunarity Slider
	m_slider[4] = std::shared_ptr<Slider>(new Slider(sliderTextCreator, "Lacunarity: ", m_sliderBGTexture, m_sliderTexture, glm::vec2(-0.75f, 0.525f), glm::vec2(0.2, 0.025), glm::vec2(0.05, 0.025), -2, 2, "Textured Shader", tmpVAO, tmpIndexCount));
	// Persistence Slider
	m_slider[5] = std::shared_ptr<Slider>(new Slider(sliderTextCreator, "Persistence: ", m_sliderBGTexture, m_sliderTexture, glm::vec2(-0.75f, 0.425f), glm::vec2(0.2, 0.025), glm::vec2(0.05, 0.025), -2, 2, "Textured Shader", tmpVAO, tmpIndexCount));
	// Octave Slider
	m_slider[6] = std::shared_ptr<Slider>(new Slider(sliderTextCreator, "Octaves: ", m_sliderBGTexture, m_sliderTexture, glm::vec2(-0.75f, 0.325f), glm::vec2(0.2, 0.025), glm::vec2(0.05, 0.025), 1, 20, "Textured Shader", tmpVAO, tmpIndexCount));
	
	// load basic 3D texture shader Vert + Frag and then compile each + Compile into shader program 
	m_shaderManager->compileShader("Textured Shader Vert", "Resources/Shaders/Vertex Shaders/textured.vert", VERTEX);
	m_shaderManager->compileShader("Textured Shader Frag", "Resources/Shaders/Fragment Shaders/textured.frag", FRAGMENT);
	
	m_shaderManager->createShaderProgram("Textured Shader");
	m_shaderManager->addShaderProgramAttribute("Textured Shader", 0, "in_Position");
	m_shaderManager->addShaderProgramAttribute("Textured Shader", 1, "in_TexCoord");
	m_shaderManager->addShaderFragOutput("Textured Shader", 0, "out_Color");
	m_shaderManager->attachShaderToProgram("Textured Shader", "Textured Shader Vert");
	m_shaderManager->attachShaderToProgram("Textured Shader", "Textured Shader Frag");
	m_shaderManager->compileShaderProgram("Textured Shader");
	
	m_shaderManager->deleteShaders();

	m_shaderManager->getShaderProgram("Textured Shader")->use();

	// size in voxels(32) + 1 is number of points in a row/column/depth(?)
	int xGSize = 33, yGSize = 33, zGSize = 33;
	float voxelSize = 1.0f;
	densityField = std::vector<std::vector<std::vector<float>>>(xGSize, std::vector<std::vector<float>>(yGSize, std::vector<float>(zGSize)));

	for (int k = 0; k < densityField[0][0].size(); ++k)
	{
		for (int j = 0; j < densityField[0].size(); ++j)
		{
			for (int i = 0;  i < densityField.size(); ++i)
			{		
				// makes a plane at a certain point then adds on perlin based fractional brownian motion
				densityField[i][j][k] = -5 + (j * 0.3125) + m_perlinNoise->fractionalBrownianMotion(glm::vec3(((float)i / 33) + 0.00001, ((float)j / 33) + 0.00001, ((float)k / 33) + 0.00001), glm::vec3(1.0f, 1.0f, 1.0f), 1.0f, 2.0, 0.65, 25, false);
			}
		}
	}
	
	m_dc = std::shared_ptr<DualContouring>(new DualContouring(m_cameraManager, m_shaderManager, m_perlinNoise, m_terrainTexture));
	
	LARGE_INTEGER ticks;
	QueryPerformanceFrequency(&ticks); // get ticks per second
	LARGE_INTEGER beforeTime, afterTime;

	QueryPerformanceCounter(&beforeTime); // get time before
	m_dc->constructSurface(32, densityField, 2.0f);
	QueryPerformanceCounter(&afterTime); // get time after

	// calculate time taken
	double time = (double)(afterTime.QuadPart - beforeTime.QuadPart) / ticks.QuadPart; 
	std::cout << "Time Taken(ms): " << time << std::endl; 

	m_voxelManager = std::shared_ptr<VoxelChunkManager>(new VoxelChunkManager(m_cameraManager, m_shaderManager, m_voxelTexture, glm::vec3(32, 32, 32), densityField));
	
	std::cout << "Number of Voxels: " << Voxel::getVoxelCount() << std::endl;

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	
	glDisable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_TRUE);
}

// function that is used to set up certain things on entry like a reset function
void DemoState::enter()
{

}

// function that draws objects to screen
void DemoState::draw()
{
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	//m_voxelManager->renderDebugBoxes(m_cubeVAO, m_cubeIndexCount); // renders chunk collision boxes
	
	// render voxels
	if (renderWorld == 0)
	{
		m_voxelManager->renderWorld();
	}
	else if (renderWorld == 1) // render terrain
	{
		m_dc->renderWorld();
	}
	else if (renderWorld == 2)
	{
		m_voxelManager->renderWorld();
		m_dc->renderWorld();
	}
	
	for (int i = 0; i < numberOfSliders; i++)
	{
		m_slider[i]->render(m_shaderManager, m_cameraManager);
	}
}

// function that is used to update the state (anything that happens while the state is occuring should go in here)
void DemoState::update()
{
	const Uint8 *keys = SDL_GetKeyboardState(NULL);
	
	bool sliderTrigger = false;

	for (int i = 0; i < numberOfSliders; i++)
	{
		if (m_slider[i]->getBoundingSquare()->detectPointCollision(glm::vec2(Mouse::getInstance()->getNDCX(), Mouse::getInstance()->getNDCY())) 
			&& Mouse::getInstance()->getLeftState() == MouseButtonState::BUTTON_STATE_DOWN)
		{
			sliderTrigger = true;
			m_slider[i]->moveSlider(Mouse::getInstance()->getNDCX());
		}
	}

	/*
	removal is in development as such its commented out
	if (sliderTrigger == false && Mouse::getInstance()->getLeftState() == MouseButtonState::BUTTON_STATE_DOWN &&
		Mouse::getInstance()->getTimeStamp() > mouseLastPressedTimer + 2500)
	{
		Ray mPick = Mouse::getInstance()->calculateRay(m_cameraManager);
		glm::vec3 indexVal;
		if (m_voxelManager->rayPickForDestruction(mPick, indexVal) == true)
		{
			m_dc->removeDensityData(indexVal, 32, densityField);
		}
		mouseLastPressedTimer = Mouse::getInstance()->getTimeStamp();
	}
	*/
	
	// if the enter key is pressed re-create the world with the slider variables
	// only recreates the voxel side of it, and its currently inactive 
	if (keys[SDL_SCANCODE_RETURN])
	{
	//	m_voxelManager->createWorldFBM(glm::vec3(m_slider[0]->getSliderValue(), m_slider[1]->getSliderValue(), m_slider[2]->getSliderValue()), m_slider[3]->getSliderValue(), m_slider[4]->getSliderValue(), m_slider[5]->getSliderValue(), std::round(m_slider[6]->getSliderValue()));
	}
		
	// Forward
	if ( keys[SDL_SCANCODE_W] ) m_cameraManager->move(0, 0, 0.1f);
	
	// Left
	if ( keys[SDL_SCANCODE_A] ) m_cameraManager->move(-0.1f, 0, 0);
	
	// Back
	if ( keys[SDL_SCANCODE_S] ) m_cameraManager->move(0, 0, -0.1f);
	
	// Right
	if ( keys[SDL_SCANCODE_D] ) m_cameraManager->move(0.1f, 0, 0);
	
	// Up
	if ( keys[SDL_SCANCODE_R] ) m_cameraManager->move(0, 0.1f, 0);
	
	// Down
	if ( keys[SDL_SCANCODE_F] ) m_cameraManager->move(0, -0.1f, 0);

	// rotate up
	if ( keys[SDL_SCANCODE_UP] ) m_cameraManager->updatePitch(0.5f);
	
    // rotate down
	if ( keys[SDL_SCANCODE_DOWN] ) m_cameraManager->updatePitch(-0.5f);

	// rotate right
	if ( keys[SDL_SCANCODE_RIGHT] || keys[SDL_SCANCODE_PERIOD] ) m_cameraManager->updateYaw(0.5f);
	
	// rotate left
	if (keys[SDL_SCANCODE_LEFT] || keys[SDL_SCANCODE_COMMA]) m_cameraManager->updateYaw(-0.5f);

	if (keys[SDL_SCANCODE_1])
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	if (keys[SDL_SCANCODE_2])
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	
	if (keys[SDL_SCANCODE_3])
		renderWorld = 0;

	if (keys[SDL_SCANCODE_4])
		renderWorld = 1;

	if (keys[SDL_SCANCODE_5])
		renderWorld = 2;

	if (keys[SDL_SCANCODE_SPACE])
	{

		// size in voxels(16) + 1 is number of points in a row/column/depth(?)
		int total = 0, xGSize = 33, yGSize = 33, zGSize = 33, left = -8, bottom = -8, front = -8;
		float voxelSize = 1.0f;
		densityField.resize(xGSize);

		for (int i = 0; i < densityField.size(); ++i)
		{
			densityField[i].resize(yGSize);

			for (int j = 0; j < densityField[i].size(); ++j)
			{
				densityField[i][j].resize(zGSize);

				for (int k = 0; k < densityField[i][j].size(); ++k)
				{
					// makes a plane at a certain point then adds on perlin based fractional brownian motion
					densityField[i][j][k] = -5 + (j * 0.3125) + m_perlinNoise->fractionalBrownianMotion(glm::vec3(((float)i / 33) + 0.00001, ((float)j / 33) + 0.00001, ((float)k / 33) + 0.00001), 
						glm::vec3(m_slider[0]->getSliderValue(), m_slider[1]->getSliderValue(), m_slider[2]->getSliderValue()), m_slider[3]->getSliderValue(), m_slider[4]->getSliderValue(), 
						   m_slider[5]->getSliderValue(), std::round(m_slider[6]->getSliderValue()), false);
				}
			}
		}

		std::cout << "Frequency X: " << m_slider[0]->getSliderValue() << std::endl;
		std::cout << "Y: " << m_slider[1]->getSliderValue() << std::endl;
		std::cout << "Z: " << m_slider[2]->getSliderValue() << std::endl;

		std::cout << "Amplitude: " << m_slider[3]->getSliderValue() << std::endl;
		std::cout << "Lacunarity: " << m_slider[4]->getSliderValue() << std::endl;
		std::cout << "Persistance: " << m_slider[5]->getSliderValue() << std::endl;
		std::cout << "Octaves" << std::round(m_slider[6]->getSliderValue()) << std::endl;
		
		m_dc->constructSurface(32, densityField, 2.0f);
		m_voxelManager->createWorldDensity(glm::vec3(32, 32, 32), densityField);

	}

}

