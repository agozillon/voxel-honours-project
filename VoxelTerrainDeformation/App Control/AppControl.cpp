#include "AppControl.h"// including game header so that we can implement the functions, and including all of the various states so that I can instantiate them and create pointers to them
#include "DemoState.h"
#include "../Mouse/Mouse.h"

#include <GL\glew.h>
#include <iostream> // for console output
#include <time.h>

// setting the instance pointer to start at null
AppControl * AppControl::m_instance = NULL;

AppControl::AppControl()
{
	m_windowSize = glm::vec2(800, 600);
	m_aspectRatio = m_windowSize.x / m_windowSize.y;
	m_demoState = new DemoState();
	m_currentState = m_demoState;
}

// a function that returns an instance of Game class, it creates one if there isn't one already. but stops creation of anymore than one 
AppControl * AppControl::getInstance()
{
	if(!m_instance)
	{
		 m_instance = new AppControl();
	}

	return m_instance;
}

// function that switches the current state to the new passed in state
void AppControl::setState(AppState* newState)
{
	if(newState != m_currentState) // if the new state is not the same as the current state change state
	{
		m_currentState = newState; // change the current State to the New state
		m_currentState->enter();
	}
}

// Set up rendering context
SDL_Window * AppControl::setupRC(SDL_GLContext &context) {
	
	SDL_Window * window;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        std::cout << "Unable to initialize SDL" << std::endl; 
	  
    // Request an OpenGL context.
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
 
    // Create 800x600 window
    window = SDL_CreateWindow("Voxel Terrain Deformation", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        m_windowSize.x, m_windowSize.y, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	
	if (!window) // Check window was created OK
		std::cout << "Unable to create window" << std::endl;
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	
	// set up TrueType / SDL_ttf
	if (TTF_Init()== -1)
		std::cout << "TTF failed to initilize";

	m_textFont = TTF_OpenFont("Resources/Fonts/MavenPro-Regular.ttf", 24);

	if (m_textFont == NULL)
		std::cout << "failed to open font";
	
	return window;
}

// function that holds the run time game loop 
void AppControl::run()
{ 
	m_hWindow = setupRC(m_glContext); // Create window and render context 

	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << std::endl;
		exit (1);
	}
	std::cout << glGetString(GL_VERSION) << std::endl;
	
	m_demoState->init();

	m_running = true; // set running to true
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (m_running)	{	// the event loop
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT)
				m_running = false;

			Mouse::getInstance()->updateEvent(sdlEvent); // updating the mouse with the new event 
		}
		
		clock_t prev = clock();
		
		m_currentState->update(); // call the update function
		m_currentState->draw(); // call the draw function
		
	//	std::cout << "FPS " << 1 / (((float)clock() - (float)prev) / CLOCKS_PER_SEC) << std::endl;

		SDL_GL_SwapWindow(m_hWindow); // swap the current frame
	}
	
}

AppControl::~AppControl()
{
	delete m_demoState;

	TTF_CloseFont(m_textFont); // clean up the SDL font
    SDL_GL_DeleteContext(m_glContext); // delete the GL context
	SDL_DestroyWindow(m_hWindow); // destroying the current window
    SDL_Quit(); // quiting from SDL
}
