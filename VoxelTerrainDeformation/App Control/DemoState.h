#pragma once

#include "AppState.h"

#include <GL\glew.h>
#include <memory>

#include "../Camera/CameraManager.h"
#include "../GUI Elements/Slider.h"
#include "../Loaders/AssimpLoader.h"
#include "../Loaders/TextureLoader.h"
#include "../Noise Generators/PerlinNoise.h"
#include "../Shader Manager/ShaderManager.h"
#include "../Texture/Texture2D.h"
#include "../Voxel Renderer/VoxelChunkManager.h"
#include "../Dual Contouring/DualContouring.h"

// Main State for this demo project, essentially renders the Voxel Terrain Deformation code
// inherits from the abstract interface AppState and the State control/management class for AppState is AppControl 
class DemoState : public AppState
{

public:
	
	DemoState();									   // constructor
	~DemoState();									   // destructor
	void init();                                       // function that is used to initilize all of the States variables and objects
	void enter();									   // function that is used to set up certain things on entry like a reset function
	void draw();                                       // function that draws objects to screen using the Game Window variable
	void update();                                     // function that is used to update the state (anything that happens while the stat is occuring should go in here)

private:

	GLuint m_cubeVAO;
	GLint m_cubeIndexCount;

	std::shared_ptr<Texture2D> m_voxelTexture;
	std::shared_ptr<Texture2D> m_terrainTexture;
	std::shared_ptr<Texture2D> m_sliderBGTexture;
	std::shared_ptr<Texture2D> m_sliderTexture;
	
	std::shared_ptr<CameraManager> m_cameraManager;
	std::shared_ptr<ShaderManager> m_shaderManager;
	std::shared_ptr<TextureLoader> m_textureLoader;
	std::shared_ptr<AssimpLoader> m_assimpLoader;
	std::shared_ptr<PerlinNoise> m_perlinNoise;
	std::shared_ptr<VoxelChunkManager> m_voxelManager;
	std::shared_ptr<DualContouring> m_dc;

	// 4096 Density values * 8 for each sign value on each edge
	std::vector<std::vector<std::vector<float>>> densityField;

	// Set of slider GUI objects used to change the values passed into
	// the voxel world creation function
	int numberOfSliders;
	int mouseLastPressedTimer;
	int renderWorld;
	std::shared_ptr<Slider> m_slider[7];
};