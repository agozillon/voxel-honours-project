#pragma once

// Abstract base class for AppState: no implementation as it is an abstract class and all of the games states shall inherit 
// from this and will require the following functions and objects, USED IN GED/AGP/RT3D JUST MODIFIED SLIGHTLY, ANYTHING NEW WILL CONTAIN
// KEYWORD UNUSED IN COMMENT
class AppState
{

public:
	// all of the functions below are pure virtual functions so any class that inherits will need to implement its own or else be classed as abstract and thus uninstantiatable 
	
	virtual ~AppState() {}                                         // blank destructor
    virtual void init() = 0;                                       // function that should be used to initilize all of the States variables and objects
	virtual void enter() = 0;									   // function that should be used to set up certain things on entry like a reset function
	virtual void draw() = 0;                                       // function that should be used to draws objects to screen 
    virtual void update() = 0;                                     // function that should be used to update the state (anything that happens while the stat is occuring should go in here)
    
};
