#pragma once

#include <GL\glew.h>

// Basic OpenGL 2D Texture Class (2D as it doesn't support any of GLs 3D Texture types)
// Holds the texture id and its target type i.e GL_TEXTURE_2D etc. this allows the user to bind
// unbind, update min/mag filters and various other functionallity with less hassle.
// Can create null textures, or pass in data to create a texture, users choice. 
// Mainly allows for a unified class to be used for all 2D Texture needs instead of having
// different functions under certain circumstances.
// OF NOTE: you still must bind your texture before updating all of the filter, wrap types etc. 
// USED IN EXTERNAL PROJECT
class Texture2D
{
public:
	// generates a null or non-null texture, detailed param list inside the .cpp
	Texture2D(GLenum targetType, GLuint width, GLuint height, GLenum internalFormat, GLenum externalFormat, GLenum dataType, GLvoid* data);
	~Texture2D();
	GLuint getTextureID(){ return m_textureID; } // returns the texture id
	
	void generateMipmap();					 // generates mip maps for the texture, texture must not be null for this to work!

	void updateMinFilter(GLenum filterType); // changes the minification filter to the passed in type
	void updateMagFilter(GLenum filterType); // changes the magnification filter to the passed in type

	void updateSWrap(GLenum wrapType);		 // changes the S-ord wrapping type to the passed in type
	void updateTWrap(GLenum wrapType);		 // changes the T-ord wrapping type to the passed in type

	void unbindTexture();					 // not really needed just sets it to 0, but maintains readabillity and ease of use having it here
	void bindTexture(GLenum activeTexture);  // binds the texture to the passed in active texture unit, I.E GL_TEXTURE0 etc.

	void deleteGLTexture();					 // deletes the GL texture from the GPU
private:
	GLuint m_textureID;
	GLenum m_bindTarget;

};
