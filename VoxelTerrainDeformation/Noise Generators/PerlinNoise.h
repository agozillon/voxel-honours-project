#pragma once

#include <glm/gtc/type_ptr.hpp>

#include "Eigen/Dense"

class PerlinNoise {

public:
	~PerlinNoise(){}
	PerlinNoise(float seed);
	float perlinNoise3D(glm::vec3 inputCoordinate, bool onlyPositive);
	float fractionalBrownianMotion(glm::vec3 inputCoordinate, glm::vec3 frequency, float amplitude, float lacunarity, float persistence, int octaves, bool onlyPositive);
	float perlinNoise3DAndGradient(glm::vec3 inputCoordinate, glm::vec3& returnGradient, bool onlyPositive);
	float fractionalBrownianMotionAndGradient(glm::vec3 inputCoordinate, glm::vec3 frequency, float amplitude, float lacunarity, float persistence, int octaves, Eigen::Vector3f& returnGradient, bool onlyPositive);

private:
	void generatePermutationTable(float seed);
	float calcGradient(int hashKey, glm::vec3 fract);
	float calcGradient(glm::vec3 cornerGradient, glm::vec3 fract);
	float refGradient(int hashKey, glm::vec3 fract);
	int getPermutation(int hashKey);
	glm::vec3 getGradient(int hashKey);
	glm::vec3 easeCurve(glm::vec3 t); 
	float lerp(float firstCoordinate, float secondCoordinate, float scalar);
	
	glm::vec3 m_gradients[12]; // edge centres of a cube 
	int m_permutations[256]; // permutation hash table with values from 0-255(technically 1-256 we offset it up by 1)
};