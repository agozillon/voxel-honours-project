#include "PerlinNoise.h"

#include <random> // for the Mersenne Twiser randomizer
#include <math.h>
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>


//@Param float seed - random value used to seed the Perlin Noise permutation table, so that we can get pseudo-random but deterministic values(i.e use the same seed, get the same values)
PerlinNoise::PerlinNoise(float seed)
{
	// Improved Perlin Noise Gradient lookup table
	// takes the "Edge Centers" of a cube and uses them as our
	// gradient, essentially like drawing crosses on a cubes edges/planes (+)
	// this is an improvement on the original because it uses a ton less 
	// look-up values for a similar(but visually more appealing, less bluring) look. 
	// splotchy/blurry look was caused by having too many gradients that were stacking 
	// up on each other in the orignial algorithm. Changed now via having less, with a far
	// more even distribution. We lose no visual quality due to the fact that human eye sight 
	// is built too blend together a large amount of varied values(orientations of surface)
	// into one large continuous value. So a ton of small variations over a dense area would
	// appear visually just the same as having a singular(averaged?) gradient over that same area.
	// used to be 256 same as the look-up table
	m_gradients[0] = glm::vec3(1.0f, 1.0f, 0.0f); // x, y, z 
	m_gradients[1] = glm::vec3(-1.0f, 1.0f, 0.0f);
	m_gradients[2] = glm::vec3(1.0f, -1.0f, 0.0f);
	m_gradients[3] = glm::vec3(-1.0f, -1.0f, 0.0f);
	m_gradients[4] = glm::vec3(1.0f, 0.0f, 1.0f);
	m_gradients[5] = glm::vec3(-1.0f, 0.0f, 1.0f);
	m_gradients[6] = glm::vec3(1.0f, 0.0f, -1.0f);
	m_gradients[7] = glm::vec3(-1.0f, 0.0f, -1.0f);
	m_gradients[8] = glm::vec3(0.0f, 1.0f, 1.0f);
	m_gradients[9] = glm::vec3(0.0f, -1.0f, 1.0f);
	m_gradients[10] = glm::vec3(0.0f, 1.0f, -1.0f);
	m_gradients[11] = glm::vec3(0.0f, -1.0f, -1.0f); 

	/*
	//Ken Perlins Improved Perlin Noise - reference table, used for testing if the algorithm is working as it should 
	// and handy to have around in general as most people just use the reference table when showing examples
	int refPermutations[256] = { 151, 160, 137, 91, 90, 15,
		131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
		190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33,
		88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
		77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244,
		102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196,
		135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123,
		5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
		223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
		129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
		251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107,
		49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
		138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180
	};
	
	for (int i = 0; i < 256; ++i)
		m_permutations[i] = refPermutations[i];
	*/

	// generates our permutation table, which we use our seed on to add the element of deterministic
	// randomness 
	generatePermutationTable(seed);
}

#pragma region Perlin Noise Functions

// Calculate the pseudorandom perlin noise value and returns it, also calculates the gradient of the noise function
// at the current point (can consider it a normal for its intent and purpose in this dual contouring method)
// @Param vec3 inputCoordinate - our co-ordinate value that we base all of our lookup table hashkeys off of 
// amongst other things.
// @Param vec3& returnGradient - this gradient gets set to the internally calculated gradient, its calculated via an analytical derivative
// @Param bool onlyPositive - keeps the values fedback in the positive range most of the time
// @Return float - returns the perlin noise value
float PerlinNoise::perlinNoise3DAndGradient(glm::vec3 inputCoordinate, glm::vec3& returnGradient, bool onlyPositive)
{
	// putting the co-ordinates in the 0 - 255 one range using modulus
	// casting to int so that we don't have to do it more than once when 
	// looking up the permutation tables in the next step, if the input co-ords
	// are outside the 256 range, you'll no longer get 0 as a lookup value (just means
	// it'll favour one of the other gradients a teeeeny wee bit more) 
	int x = (int)std::floor(inputCoordinate.x) % 256;
	int y = (int)std::floor(inputCoordinate.y) % 256;
	int z = (int)std::floor(inputCoordinate.z) % 256;

	// getting our gradiant keys for our cube grid by looking up the permutation table 6 times.
	// 2 for X, 2 for Y, 2 for Z (I believe), Basically just to pseudorandomly index our cube gradients.
	int A = getPermutation(x) + y;
	int AA = getPermutation(A) + z;
	int AB = getPermutation(A + 1) + z;
	int B = getPermutation(x + 1) + y;
	int BA = getPermutation(B) + z;
	int BB = getPermutation(B + 1) + z;

	// Get the passed in position relative to the cube, the cube is centered at 0, 0, 0
	// and goes from -1 to 1. Think of it as being a huge grid and every int value 
	// is another grid section so we're only ever interested in the fractional part 
	// i.e x 81.84, we don't care about the 81 we just care about the .84 it's at 0.84
	// on the hypothetical and irrelevant x axis 81st cube grid
	glm::vec3 fractRemainder(inputCoordinate.x - std::floor(inputCoordinate.x),
		inputCoordinate.y - std::floor(inputCoordinate.y), inputCoordinate.z - std::floor(inputCoordinate.z));

	// We now fit our fractRemainder to the ease curve which will exagerate it's bias to the 0 or 1
	// range for later scaling/weighting purposes during the LERP averaging. Basically if the value is closer
	// to 0, the output will be closer to the first passed in LERP value. If you look below all of the first passed in Lerp values
	// are closer to the "Right" +1 side of the cube and the secondary are the "Left"(Well not exactly, there gradients so it just favours
	// other gradients over others but it helps you visualize). So Basically 0 Favours one side of the cube
	// more than the other and vice versa. We want to exagerate this bias with the ease curve as it has a smoothing effect on the final noise
	// output, I'm not 100% sure on the this, but I assume having values too closely favouring both gradients is going to have some 
	// unusual back and forth between gradients causing it to look rough.
	glm::vec3 interp = easeCurve(fractRemainder);
	
	glm::vec3 derivative;
	derivative.x = 30 * fractRemainder.x * fractRemainder.x * (fractRemainder.x - 1) * (fractRemainder.x - 1);
	derivative.y = 30 * fractRemainder.y * fractRemainder.y * (fractRemainder.y - 1) * (fractRemainder.y - 1);
	derivative.z = 30 * fractRemainder.z * fractRemainder.z * (fractRemainder.z - 1) * (fractRemainder.z - 1);
	
	glm::vec3 perm000 = getGradient(getPermutation(AA));
	glm::vec3 perm100 = getGradient(getPermutation(BA)); 
	glm::vec3 perm010 = getGradient(getPermutation(AB));
	glm::vec3 perm110 = getGradient(getPermutation(BB));
	glm::vec3 perm001 = getGradient(getPermutation(AA + 1)); 
	glm::vec3 perm101 = getGradient(getPermutation(BA + 1));
	glm::vec3 perm011 = getGradient(getPermutation(AB + 1));
	glm::vec3 perm111 = getGradient(getPermutation(BB + 1));
	
	float dot000 = calcGradient(perm000, fractRemainder);
	float dot100 = calcGradient(perm100, fractRemainder - glm::vec3(1, 0, 0));
	float dot010 = calcGradient(perm010, fractRemainder - glm::vec3(0, 1, 0));
	float dot110 = calcGradient(perm110, fractRemainder - glm::vec3(1, 1, 0));
	float dot001 = calcGradient(perm001, fractRemainder - glm::vec3(0, 0, 1));
	float dot101 = calcGradient(perm101, fractRemainder - glm::vec3(1, 0, 1));
	float dot011 = calcGradient(perm011, fractRemainder - glm::vec3(0, 1, 1));
	float dot111 = calcGradient(perm111, fractRemainder - glm::vec3(1, 1, 1));


   // code to calculate the gradient of perlin noise (relevant to calculating the normals for Dual Contouring)
   // uses an analytical derivative found here: http://stackoverflow.com/questions/4297024/3d-perlin-noise-analytical-derivative
   // for quickness, so not my maths. As far as I know its neither an finite or central difference method, however 
   // the author claims it to be quicker. 
	
	returnGradient.x = perm000.x 
	+ derivative.x * (dot100 - dot000)
	+ interp.x * (perm100.x - perm000.x)
	+ interp.y * (perm010.x - perm000.x) 
	+ interp.z * (perm001.x - perm000.x)
	+ (derivative.x * interp.y) * (dot110 - dot010 - dot100 + dot000)
	+ (interp.x * interp.y) * (perm110.x - perm010.x - perm100.x + perm000.x) 
	+ (derivative.x * interp.z) * (dot101 - dot001 - dot100 + dot000)
	+ (interp.x * interp.z) * (perm101.x - perm001.x - perm100.x - perm000.x)
	+ (interp.y * interp.z) * (perm011.x - perm001.x - perm010.x + perm000.x)
	+ (derivative.x * interp.y * interp.z) * (dot111 - dot011 - dot101 + dot001 - dot110 + dot010 + dot100 - dot000)
	+ (interp.x * interp.y * interp.z) * (perm111.x - perm011.x - perm101.x + perm001.x - perm110.x + perm010.x + perm100.x - perm000.x);
	
	returnGradient.y = perm000.y 
	+ interp.x * (perm100.y - perm000.y)
	+ derivative.y * (dot010 - dot000)
	+ interp.y * (perm010.y - perm000.y)
	+ interp.z * (perm001.y - perm000.y)
	+ (interp.x * derivative.y) *(dot110 - dot010 - dot100 + dot000)
	+ (interp.x * interp.y) * (perm110.y - perm010.y - perm100.y + perm000.y)
	+ (interp.x * interp.z) * (perm101.y - perm001.y - perm100.y + perm000.y)
	+ (derivative.y * interp.z) * (dot011 - dot001 - dot010 + dot000)
	+ (interp.y * interp.z) * (perm011.y - perm001.y - perm010.y + perm000.y)
	+ (interp.x * derivative.y * interp.z) * (dot111 - dot011 - dot101 + dot001 - dot110 + dot010 + dot100 - dot000)
	+ (interp.x * interp.y * interp.z) * (perm111.y - perm011.y - perm101.y + perm001.y - perm110.y + perm010.y + perm100.y - perm000.y);

	returnGradient.z = perm000.z
	+ interp.x * (perm100.z - perm000.z)
	+ interp.y * (perm010.z - perm000.z)
	+ derivative.z * (dot001 - dot000)
	+ interp.z * (perm001.z - perm000.z)
	+ (interp.x * interp.y) * (perm110.z - perm010.z - perm100.z + perm000.z)
	+ (interp.x * derivative.z) * (dot101 - dot001 - dot100 + dot000)
	+ (interp.x * interp.z) * (perm101.z - perm001.z - perm100.z + perm000.z)
	+ (interp.y * derivative.z) * (dot011 - dot001 - dot010 + dot000)
	+ (interp.y * interp.z) * (perm011.z - perm001.z - perm010.z + perm000.z)
	+ (interp.x * interp.y * derivative.z) * (dot111 - dot011 - dot101 + dot001 - dot110 + dot010 + dot100 - dot000)
	+ (interp.x * interp.y * interp.z) * (perm111.z - perm011.z - perm101.z + perm001.z - perm110.z + perm010.z + perm100.z - perm000.z);
	
	// the addition of the negative values may be to bring the values all to the same point or make sure that there is 
	// no possibility of a -ve gradient
	// Linearly interpolating 4x on the x axis, then these x values 2x on the y axis and then the y output values
	// once on the z axis. So Visualize a cube, we start at the 8 corners and interpolate across the cube on there x's so
	// front-top-right corner to front-top-left, front-bottom-right to front-bottom-left etc. then we use the 4 interpolated values
	// to interpolate across the y-axis(these points would now be some arbitrary point on the x-axis), so now we're left with 2 final
	// values given from the y-lerp and we lerp these across the z-axis(There is a nice picture on GPU Gems Chapter 5 Improved Perlin Noise
	// If you have trouble visualizing this). The Negation of the co-ordinate values from the fract remainder is to get the distance from the
	// corners of each cube to our co-ordinate inside the 1x1 cube, since one of our corners are the origin point 0,0,0 we don't need
	// to negate one of the values and can instead just place the fract remainder! The calcGradient is just the dot product of the gradient
	// found at the permutation value in the table against the position - corner value (value from corner to the point)
	float noise = lerp(
		lerp(lerp(dot000,
		dot100, interp.x), lerp(dot010, dot110, interp.x), interp.y),
		lerp(lerp(dot001, dot101, interp.x), lerp(dot011, dot111, interp.x), interp.y),
		interp.z);
	
	if (!onlyPositive)
	{
		// the magic number is 1 / (0.75), which essentially scales the perlin noise to the -1 to 1 range, 
		// otherwise it goes out of the range, the calculation comes from working out the max possible value
		// perlin noise can output (when the input co-ordinate is in the center and all the corner gradients point directly towards it)
		return noise * 1.15470054f;
	}
	else
		return (noise + 1.0f) / 2.0f; // add 1 so we bump all -1 (or the majority, since some values go outside the expected range) to positive
	// and then divide by 2 so we squash values into the 0-1 range i.e values that were positive already don't
	// end up > 1 (same as with -ves some positives will end up greater than)

}

// Just generated Perlin Noise on its own, doesn't try calculate the gradient via an analytical derivative
// the gradient calculation in the final trilinear lerp is interchangeable between refGradient(perlins reference gradient
// calculation) and calcGradient (simply a dot product calculation) you get mildly different results from both
// @Param vec3 inputCoordinate - our co-ordinate value that we base all of our lookup table hashkeys off of 
// amongst other things.
// @Param bool onlyPositive - keeps the values fedback in the positive range most of the time
// side effect is that it decreases the range of Perlin noise to mostly 0 - 1
// @Return 
float PerlinNoise::perlinNoise3D(glm::vec3 inputCoordinate, bool onlyPositive)
{
	// putting the co-ordinates in the 0 - 255 one range using modulus
	// casting to int so that we don't have to do it more than once when 
	// looking up the permutation tables in the next step, if the input co-ords
	// are outside the 256 range, you'll no longer get 0 as a lookup value (just means
	// it'll favour one of the other gradients a teeeeny wee bit more) 
	int x = (int)std::floor(inputCoordinate.x) % 256;
	int y = (int)std::floor(inputCoordinate.y) % 256;
	int z = (int)std::floor(inputCoordinate.z) % 256;

	// getting our gradiant keys for our cube grid by looking up the permutation table 6 times.
	// 2 for X, 2 for Y, 2 for Z (I believe), Basically just to pseudorandomly index our cube gradients.
	int A = getPermutation(x) + y;
	int AA = getPermutation(A) + z;
	int AB = getPermutation(A + 1) + z;
	int B = getPermutation(x + 1) + y;
	int BA = getPermutation(B) + z;
	int BB = getPermutation(B + 1) + z;

	// Get the passed in position relative to the cube, the cube is centered at 0, 0, 0
	// and goes from -1 to 1. Think of it as being a huge grid and every int value 
	// is another grid section so we're only ever interested in the fractional part 
	// i.e x 81.84, we don't care about the 81 we just care about the .84 it's at 0.84
	// on the hypothetical and irrelevant x axis 81st cube grid
	glm::vec3 fractRemainder(inputCoordinate.x - std::floor(inputCoordinate.x), 
		inputCoordinate.y - std::floor(inputCoordinate.y), inputCoordinate.z - std::floor(inputCoordinate.z));
	
	// We now fit our fractRemainder to the ease curve which will exagerate it's bias to the 0 or 1
	// range for later scaling/weighting purposes during the LERP averaging. Basically if the value is closer
	// to 0, the output will be closer to the first passed in LERP value. If you look below all of the first passed in Lerp values
	// are closer to the "Right" +1 side of the cube and the secondary are the "Left"(Well not exactly, there gradients so it just favours
	// other gradients over others but it helps you visualize). So Basically 0 Favours one side of the cube
	// more than the other and vice versa. We want to exagerate this bias with the ease curve as it has a smoothing effect on the final noise
	// output, I'm not 100% sure on the this, but I assume having values too closely favouring both gradients is going to have some 
	// unusual back and forth between gradients causing it to look rough.
	glm::vec3 interp = easeCurve(fractRemainder);
	
	// the addition of the negative values may be to bring the values all to the same point or make sure that there is 
	// no possibility of a -ve gradient
	// Linearly interpolating 4x on the x axis, then these x values 2x on the y axis and then the y output values
	// once on the z axis. So Visualize a cube, we start at the 8 corners and interpolate across the cube on there x's so
	// front-top-right corner to front-top-left, front-bottom-right to front-bottom-left etc. then we use the 4 interpolated values
	// to interpolate across the y-axis(these points would now be some arbitrary point on the x-axis), so now we're left with 2 final
	// values given from the y-lerp and we lerp these across the z-axis(There is a nice picture on GPU Gems Chapter 5 Improved Perlin Noise
	// If you have trouble visualizing this). The Negation of the co-ordinate values from the fract remainder is to get the distance from the
	// corners of each cube to our co-ordinate inside the 1x1 cube, since one of our corners are the origin point 0,0,0 we don't need
	// to negate one of the values and can instead just place the fract remainder! The calcGradient is just the dot product of the gradient
	// found at the permutation value in the table against the position - corner value (value from corner to the point)
	float noise = lerp(
		lerp(lerp(refGradient(getPermutation(AA), fractRemainder),
				refGradient(getPermutation(BA), fractRemainder - glm::vec3(1, 0, 0)), interp.x),
			lerp(refGradient(getPermutation(AB), fractRemainder - glm::vec3(0, 1, 0)),
				refGradient(getPermutation(BB), fractRemainder - glm::vec3(1, 1, 0)), interp.x), interp.y),
		lerp(lerp(refGradient(getPermutation(AA + 1), fractRemainder - glm::vec3(0, 0, 1)),
				refGradient(getPermutation(BA + 1), fractRemainder - glm::vec3(1, 0, 1)), interp.x),
			lerp(refGradient(getPermutation(AB + 1), fractRemainder - glm::vec3(0, 1, 1)),
				refGradient(getPermutation(BB + 1), fractRemainder - glm::vec3(1, 1, 1)), interp.x), interp.y),
		interp.z);

	if (!onlyPositive)
		return noise;
	else
		return (noise + 1.0f) / 2.0f; // add 1 so we bump all -1 (or the majority, since some values go outside the expected range) to positive
									  // and then divide by 2 so we squash values into the 0-1 range i.e values that were positive already don't
									  // end up > 1 (same as with -ves some positives will end up greater than)
									
}

/*
	Fractional Brownian Motion sometimes acronymed as fBM a way of combining noise(different types, varying levels etc.) into a single combined noise
	in this case it specifically adds different octaves of 3D Perlin noise
	
	@param vec3 inputCoordinate - input co-ordinate value, works the same as if giving a point to 3D Perlin, essentially sample noise at this point
	@param vec3 frequency - increases the frequency of each octave of noise, gets increased by lacunarity after each octave of noise
	frequency can be seen as the space between each noise hump, higher frequency more points.
	@param float amplitude - increases the amplitude of each noise point for all octaves, increasing the regular value outputted
	by the noise, it gets reduced each octave so noise(0.5) * amiplitude(0.25) = 0.25
	@param float lacunarity - varies the frequency by multiplying this number against it after each octave has been added on
	effectively making each level of noise more "zoomed out" or "zoomed in" or with more/less noise points
	@param float persistence - varies the amplitude each octave same way as the frequency, essentially increases the noise troughs
	in size or reduces depending on the value you put in
	@param bool onlyPositive - feeds into the 3D Perlin Noise function and keeps the values fedback from that in the positive range most of the time
	side effect is that it decreases the range of Perlin noise to mostly 0-1
	@return float - returns the combined noise value at this point
*/
float PerlinNoise::fractionalBrownianMotion(glm::vec3 inputCoordinate, glm::vec3 frequency, float amplitude, float lacunarity, float persistence, int octaves, bool onlyPositive)
{
	float noiseTotal = 0;

	for (int i = 0; i < octaves; ++i)
	{
		noiseTotal += perlinNoise3D(inputCoordinate * frequency, onlyPositive) * amplitude;
		frequency = frequency * lacunarity;
		amplitude = amplitude * persistence;
	}

	return noiseTotal;
	
}


/*
	 Same as the other Fractional Brownian Motion (so can read the details there) except that this one attempts to calculate the gradient via adding up the multiple octaves
	 and then normalizing them, it then returns it via the returnGradient parameter 
*/
float PerlinNoise::fractionalBrownianMotionAndGradient(glm::vec3 inputCoordinate, glm::vec3 frequency, float amplitude, float lacunarity,
	float persistence, int octaves, Eigen::Vector3f& returnGradient, bool onlyPositive)
{
	float noiseTotal = 0;

	// estimating a gradient using the Central Differencing Method (from what I understand), fairly expensive, but its the most accurate normal
	// calculation I could find, the analytical derivative was causing severe artifacts (and apparently doesn't scale too great with FBM and 
	// noise mixing) tried forward differencing (or what i believe to be) was less accurate, but required less function calls so would be mildy quicker
	returnGradient(0) = perlinNoise3D(glm::vec3(inputCoordinate[0] + 0.001f, inputCoordinate[1], inputCoordinate[2]), false)
		- perlinNoise3D(glm::vec3(inputCoordinate[0] - 0.001f, inputCoordinate[1], inputCoordinate[2]), false);

	returnGradient(1) = perlinNoise3D(glm::vec3(inputCoordinate[0], inputCoordinate[1] + 0.001, inputCoordinate[2]), false)
		- perlinNoise3D(glm::vec3(inputCoordinate[0], inputCoordinate[1] - 0.001, inputCoordinate[2]), false);

	returnGradient(2) = perlinNoise3D(glm::vec3(inputCoordinate[0], inputCoordinate[1], inputCoordinate[2] + 0.001f), false)
		- perlinNoise3D(glm::vec3(inputCoordinate[0], inputCoordinate[1], inputCoordinate[2] - 0.001f), false);

	for (int i = 0; i < octaves; ++i)
	{
		noiseTotal += perlinNoise3D(inputCoordinate * frequency, onlyPositive) * amplitude;
		frequency = frequency * lacunarity;
		amplitude = amplitude * persistence;
	}
	
	return noiseTotal;
}

#pragma endregion


#pragma region Helper Functions

/*
Improved Perlin Noise "fade" function(it's an ease curve, both old and imp, imp is just more exagerated/curvy)
- 6t^5 - 15t^4 + 10t^3 it's a 5th order the old version was a cubic interpolation function and had (second order) discontinuiies at
t = 0 and t = 1 meaning that they didn't link up in a constant continuos manner (in a second order
case it means it goes to infinity/doesn't exist/has a large gap that can't be connected)
this caused lighting artifacts when using it for bump/normal mapping as for using it with
terrain generation I assume it'd cause similar funky artifacts without taking the issue into account.
The improved perlin noise version doesn't have this issue that's in use here doesn't have this issue.

What the function does, is fit/smooth the value to the ease curve which moves the value closer to 0 when it's
nearer 0 and closer to 1 when it's closer to 1, it exagerates it's bias essentially in the 0 - 1 range (at least when the pass
in is in the 0 - 1 range)

@Param vec3 t - co-ordinate to pass in and fit to the curve, in the case of perlin noise a remainder of the input co-ordinate.
@Return vec3 - the co-ordinate fitted the the function
*/
glm::vec3 PerlinNoise::easeCurve(glm::vec3 t)
{
	// Improved Perlin noise curve function, it's a simplified and rearanged form of
	// the fith-degree interpolator 6t^5 - 15t^4 + 10t^3 the simplification
	// is for performance reasons ( I didn't perform the simplification it's a standard Imp Perlin Noise
	// algorithm line that I found on the GPU Gems Chapter 5 & 26 article and is a major part of the algorithm) 
	// this line replaces the simplified version of 3t^2 - 2t^3 which was the original perlin noise
	// interpolation function
	return t*t*t*(t*(t * 6.0f - 15.0f) + 10.0f);
}

// Simple linear interpolation function for use within the Perlin Noise class
// @Params float firstCoordinate - any float value you wish to be your initial value to linearly interpolate from
// @Params float secondCoordinate - any float value you wish to be your second value to linearly interpolate to
// @Params float scalar - the % fraction to interpolate by, i.e 0.5 so half way between the first ordinate and second ordinate
// @Return float - the calculated interpolated value
float PerlinNoise::lerp(float firstCoordinate, float secondCoordinate, float scalar)
{
	return firstCoordinate + (secondCoordinate - firstCoordinate) * scalar;
}

// Look up of the gradient table that also calculates the dot product of the gradient found in the table
// and the vec passed in
// @Param int hashKey - integer value passed in to look up the table of gradients, should be in the 0 - 255 range
// @Param int fract - vec3 value that we project our gradient onto to get our return gradient
// @Return - the dot product calculated gradient of the original co-ordinate fraction projected onto our Perlin Noise Gradient Cube
float PerlinNoise::calcGradient(int hashKey, glm::vec3 fract)
{
	return glm::dot(m_gradients[hashKey % 12], fract);
}

// Look up of the gradient table that also calculates the dot product of the gradient found in the table
// and the vec passed in
// @Param glm::vec3 cornerGradient - gradient value passed in to dot product agaisnt the passed in fract position, should be a gradient from the 
// perlin noise graident table
// @Param int fract - vec3 value that we project our gradient onto to get our return gradient
// @Return - the dot product calculated gradient of the original co-ordinate fraction projected onto our Perlin Noise Gradient Cube
float PerlinNoise::calcGradient(glm::vec3 cornerGradient, glm::vec3 fract)
{
	return glm::dot(cornerGradient, fract);
}

// just looks into the m_gradients vec3 array with the hash key (keeping the hash key within the arrays range)
// and returns it
// @Param int hashKey - integer value passed in to look up the table of gradients, should be in the 0 - 255 range
// @Return - returns the looked up gradient value from the gradient table
glm::vec3 PerlinNoise::getGradient(int hashKey)
{
	return m_gradients[hashKey % 12];
}

// The below function is taken straight from Ken Perlins Improved Perlin Noise java reference algorithm, I originally used it
// for test purposes, but have kept it as it works differently to calcGradient (specified in GPU Gems 2 Chapter 26) and generates
// different noise, it uses bit twiddling instead of a gradient table to create a value based on the input co-ord! Copyright 2002 of Ken Perlin 
// @Param int hashKey - integer value passed in to look up the table of gradients, should be in the 0 - 255 range
// @Param int fract - vec3 value that we project our gradient onto to get our return gradient
// @Return float - the calculated gradient 
float PerlinNoise::refGradient(int hash, glm::vec3 coord) {
	int h = hash & 15;
	// Convert lower 4 bits of hash inot 12 gradient directions
	double u = h < 8 ? coord.x : coord.y,
		v = h < 4 ? coord.y : h == 12 || h == 14 ? coord.x : coord.z;
	return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}

// Simple function that gets the permutation value from the array, if the value is > 255 (array size is 256)
// or less than 0, it'll fix the hashKey value back into the 0 - 255 range and return that value
// @Param int hashKey - passed in integer value that we use to access the permutation array
// @Return int - returns the permutation value from the permutations array
// stored at the passed in hashKey
int PerlinNoise::getPermutation(int hashKey)
{
	if (hashKey > 255)
		hashKey = hashKey % 256;

	if (hashKey < 0)
		hashKey = 255 + hashKey;

	return m_permutations[hashKey];
}


// Really simple function to generate the 256 value permutation table, takes in a seed that's a random
// float to help pseudo-randomly generate our permutation table, which adds the pseudo-randomness
// to our Perlin Noise generation. It's intended for non-real time generation i.e generate the table once
// it's not really optimized for constant calling.
// @param float seed - seed value for randomizing the permutation table. It's deterministic so the same value will get the same results
void PerlinNoise::generatePermutationTable(float seed)
{
	std::mt19937 gen(seed); // creating & seeding our Mersenne Twister random number generator
	std::uniform_int_distribution<> distrib(0, 255); // keep the seeding between 0 and 255
	int i = 0; // counter for the current size of the permutations array
	
	while (i < 256)
	{
		bool newValue = true; // is it a newValue...we only accept unique values from 0 - 255
		int tmpPerm = distrib(gen); // get our seeded random value locked into our permutation range
		
		// check if the randomly generated values already in our permutations array 
		for (int j = 0; j < i; j++)
		{
			if (m_permutations[j] == tmpPerm)
			{
				newValue = false;
				break;
			}
		}

		// add the value on if it's new and increment the permutation counter
		if (newValue == true)
		{
			m_permutations[i] = tmpPerm;
			i++;
		}
	}
}

#pragma endregion