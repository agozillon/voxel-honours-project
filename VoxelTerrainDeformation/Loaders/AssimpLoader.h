#pragma once

#include <string>
#include <GL/glew.h>

// simple Assimp model loader, loads in a static model at the moment and doesn't handle
// animation yet. Also currently creates the VAO/VBOs for the model internally then passes the vao handle and index count out.
// can handle the loading in of any Assimp supported static model. SIMILAR TO 3RD YEAR GROUP PROJECT(I made that one as well though..) 
class AssimpLoader
{
public:

	AssimpLoader(){}
	~AssimpLoader(){}
	
	// Currently put in the file path to the model, pass in two empty variables a GLuint and GLint 
	// that will get set to the new models VAO and index count! You can then use these to render a mesh  
	void load(char const* filePath, GLuint &getVAO, GLint &getIndexCount);

private:
	// binds all the relevant VBO's verts/tex/normals/indices into the models VAO
	GLuint bind(const GLfloat* vertices, GLint vertCount, const GLfloat* texCoords, GLint texCount, const GLuint* indices, GLint indiceCount, const GLfloat* normals, GLint normalCount) const;

	// binds some data to a vertex buffer object based on various GL parameters and then passes out a GLuint
	// @data - data to bind
	// @attribIndex - GLuint value that specifies the shader bind point, Layout (Location = 0) etc.
	// @targetType - type of buffer the data should be put into GL_ARRAY_BUFFER etc.
	// @drawMode - hint to GL at what the buffer will be used for, i.e GL_STATIC_DRAW for data not being constantly rewritten too
	// @dataType - type of data that's being passed in i.e GLfloat etc.
	// @normalized - should the data be normalized before accessing or remain the same
	// @sizeOfData - size in bytes of the full data block
	// @numberOfDataPerValue - number of data pieces per value I.E 3 floats per vertex for the x, y, z of the vertex
	GLuint bindVBO(const GLvoid* data, GLint attribIndex, GLenum targetType, GLenum drawMode, GLenum dataType, GLboolean normalized, size_t sizeOfData, GLuint numberOfDataPerValue) const;
};
