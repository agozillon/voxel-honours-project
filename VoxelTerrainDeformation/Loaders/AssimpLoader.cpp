#include "AssimpLoader.h"

#include <iostream>
#include <vector>
#include <postprocess.h>
#include <scene.h>
#include <Importer.hpp>


// basic assimp load function for loading in a single static model 
void AssimpLoader::load(char const* filePath, GLuint &getVAO, GLint &getIndexCount)
{
	Assimp::Importer importer;
    
	// reads in the file to a scene object
	const aiScene* scene = importer.ReadFile(filePath, aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs);
	
	std::vector<float> vertices;
	std::vector<unsigned int> indices;
	std::vector<float> normals;
	std::vector<float> texCoords;

	// if it downloaded appropriately
	if(scene)
	{
		const aiMesh * mesh = scene->mMeshes[0]; // get the first mesh in the loaded in scene, (only one model in loaded in file then its that model)
		const aiVector3D blankVal(0.0f, 0.0f, 0.0f);
		
		// get the verts, nomrals and tex coords of the mesh in the scene and push them
		// into a vector for each type
		for(unsigned int i = 0; i < mesh->mNumVertices; i++){
			const aiVector3D * vert = &(mesh->mVertices[i]);
			const aiVector3D * normal = &(mesh->mNormals[i]);
			const aiVector3D * texCoord;

			if(mesh->HasTextureCoords(0)){
				texCoord = &(mesh->mTextureCoords[0][i]);
			}else{
				texCoord = &blankVal;
			}

			vertices.push_back(vert->x);
			vertices.push_back(vert->y);
			vertices.push_back(vert->z);

			texCoords.push_back(texCoord->x);
			texCoords.push_back(texCoord->y);
			
			normals.push_back(normal->x);
			normals.push_back(normal->y);
			normals.push_back(normal->z);
		}
	
		// get the indices of the model, make sure there is 3 per face so we're missing no data
		for(unsigned int i = 0; i < mesh->mNumFaces; i++){
			const aiFace& face = mesh->mFaces[i];
				
			if(face.mNumIndices != 3)
				std::cout << "Possible Error: Incorrect number of indices for face " << std::endl;
				
			indices.push_back(face.mIndices[0]);
			indices.push_back(face.mIndices[1]);
			indices.push_back(face.mIndices[2]);
		}
	}else{
		std::cout << "model unloadable, non-existant or bad filepath " << std::endl;
	}

	// create a mesh with this data and then return it
	getVAO = bind(&vertices[0], (GLint)vertices.size(), &texCoords[0], (GLint)texCoords.size(), &indices[0], (GLint)indices.size(),&normals[0], (GLint)normals.size());
	getIndexCount = (GLint)indices.size();

}

// very simple VAO model bind function, intend to extend for more flexibility and packing all the model data into one
// VAO!
GLuint AssimpLoader::bind(const GLfloat* vertices, GLint vertCount, const GLfloat* texCoords, GLint texCount, const GLuint* indices, GLint indiceCount, const GLfloat* normals, GLint normalCount) const
{
	GLuint vao;

	// bind VAO
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    

	// if any of the below are above 0 then bind them
	if (vertCount > 0)
		bindVBO(vertices, 0, GL_ARRAY_BUFFER, GL_STATIC_DRAW, GL_FLOAT, GL_FALSE, sizeof(GLfloat)* vertCount, 3);

	if (texCount > 0)
		bindVBO(texCoords, 1, GL_ARRAY_BUFFER, GL_STATIC_DRAW, GL_FLOAT, GL_FALSE, sizeof(GLfloat)* texCount, 2);

	if (normalCount > 0)
		bindVBO(normals, 2, GL_ARRAY_BUFFER, GL_STATIC_DRAW, GL_FLOAT, GL_FALSE, sizeof(GLfloat)* normalCount, 3);

	// of note -1 in the vertex attrib pointer = no vertex attribute 
	if (indiceCount > 0)
		bindVBO(indices, -1, GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW, GL_UNSIGNED_INT, GL_FALSE, sizeof(GLuint)* indiceCount, 2);
	
	// unbind
    glBindVertexArray(0);
    
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	return vao;
}

// just a simple encapsulated (VBO) buffer binding function, simplifies binding buffers
GLuint AssimpLoader::bindVBO(const GLvoid* data, GLint attribIndex, GLenum targetType, GLenum drawMode, GLenum dataType, GLboolean normalized, size_t sizeOfData, GLuint numberOfDataPerValue) const
{
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(targetType, vbo);
	glBufferData(targetType, sizeOfData, data, drawMode);

	// if it has a -ve attrib index then its not a normal attribute type, such as indice values that use GL_ELEMENT_ARRAY_BUFFER!
	if (attribIndex >= 0)
	{
		glEnableVertexAttribArray(attribIndex);
		glVertexAttribPointer(attribIndex, numberOfDataPerValue, dataType, normalized, 0, 0);
	}
	
	return vbo;
}