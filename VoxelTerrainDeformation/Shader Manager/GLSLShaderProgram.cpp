#include "GLSLShaderProgram.h"

#include <iostream>
#include <string>
#include <glm/gtc/type_ptr.hpp>



GLSLShaderProgram::~GLSLShaderProgram()
{
	glDeleteProgram(m_shaderProgramID); // delete the shader program from the GPU
}

GLuint GLSLShaderProgram::getUniformLocation(const char* uniformName) const
{
	return glGetUniformLocation(m_shaderProgramID, uniformName);
}


// sends a single float to the shader program
void GLSLShaderProgram::setUniform1f(const char* uniformName, GLfloat data) const
{
	glUniform1f(getUniformLocation(uniformName), data);
}

// sends a vec2 to the shader program
void GLSLShaderProgram::setUniform2fv(const char* uniformName, int count, const GLfloat* data) const
{
	glUniform2fv(getUniformLocation(uniformName), count, data);
}

// sends a vec3 to the shader program
void GLSLShaderProgram::setUniform3fv(const char* uniformName, int count, const GLfloat* data) const
{
	glUniform3fv(getUniformLocation(uniformName), count, data);
}

// sends a vec4 to the shader program
void GLSLShaderProgram::setUniform4fv(const char* uniformName, int count, const GLfloat* value) const
{
	glUniform4fv(getUniformLocation(uniformName), count, value);
}

// sends a 3x3 matrix to the shader program
void GLSLShaderProgram::setUniformMatrix3fv(const char* uniformName, int count, GLboolean transpose, const GLfloat* data) const
{
	glUniformMatrix3fv(getUniformLocation(uniformName), count, transpose, data);
}

// sends a 4x4 matrix to the shader program
void GLSLShaderProgram::setUniformMatrix4fv(const char* uniformName, int count, GLboolean transpose, const GLfloat* data) const
{
	glUniformMatrix4fv(getUniformLocation(uniformName), count, transpose, data);
}

// get a uniform name and send an integer to the GLSLShaderProgram
void GLSLShaderProgram::setUniformi(const char* uniformName, GLuint data) const
{
	glUniform1i(getUniformLocation(uniformName), data);
}

// adds an attribute onto the map
void GLSLShaderProgram::addAttribute(GLuint index, const char* attribName)
{
	m_attribList[index] = attribName;
}

// sets up the default input attributes for the map that
// most shaders use, position, colour(might remove), normal, tex coords
void GLSLShaderProgram::addDefaultAttributes()
{
	m_attribList[0] = "in_Position";
	m_attribList[1] = "in_Color";
	m_attribList[2] = "in_Normal";
	m_attribList[3] = "in_TexCoord";
}

// clears the current attribute map (doesn't do anything to the shader program after compilation
// just clears the map holding the data)
void GLSLShaderProgram::clearAttributes()
{
	m_attribList.clear();
}

// adds an output for the shader program (fragment stage output, i.e colour)
void GLSLShaderProgram::addFragOutput(GLuint index, const char* outputName)
{
	m_fragOutputList[index] = outputName;
}

// adds the defualt frag output, colour (out_Color)
void GLSLShaderProgram::addDefaultOutput()
{
	m_fragOutputList[0] = "out_Color";
}

// clears the fragment output map (doesn't do anything to the shader program after compilation
// just clears the map holding the data)
void GLSLShaderProgram::clearFragOutput()
{
	m_fragOutputList.clear();
}

// attach a shader to the shader program, when compiled all of the attached shaders will make up the shader program
// I.E attach a phong.vert and a phong.frag shader to create a phong shader 
void GLSLShaderProgram::attachShader(GLuint shaderID)
{
	m_attachedShaders.push_back(shaderID);
}

// compiles the shader program, should only be called once
void GLSLShaderProgram::compileShaderProgram()
{
	m_shaderProgramID = glCreateProgram();

	// attaching all the various shader stages together to create the program
	for (size_t i = 0; i < m_attachedShaders.size(); ++i)
		glAttachShader(m_shaderProgramID, m_attachedShaders[i]);

	// loop through current attriblist and use for this shader link
	for (std::map<const GLuint, const char*>::iterator it = m_attribList.begin(); it != m_attribList.end(); ++it)
		glBindAttribLocation(m_shaderProgramID, it->first, it->second);
	
	// loop through current outputList and use for this shader link
	for (std::map<const GLuint, const char*>::iterator it = m_fragOutputList.begin(); it != m_fragOutputList.end(); ++it)
    {
        glBindFragDataLocation(m_shaderProgramID, it->first, it->second);
    }
	
	// link the shader stages to make the program and then use it 
	glLinkProgram(m_shaderProgramID);
	glUseProgram(m_shaderProgramID);
    
   // std::cout << "Shader did not compile." << std::endl;
    GLint maxLength = 0;
    glGetShaderiv(m_shaderProgramID, GL_INFO_LOG_LENGTH, &maxLength);
    
    //The maxLength includes the NULL character
    std::vector<char> errorLog(256);
    glGetShaderInfoLog(m_shaderProgramID, 512, &maxLength, &errorLog[0]);
    
    for(std::vector<char>::iterator it = errorLog.begin(); it != errorLog.end(); ++it) {
        std::cout << *it;
    }
	bool glerror = false; while (glerror == false) { GLenum error = glGetError(); std::cout << error << std::endl; if (error == GL_NO_ERROR) glerror = true; }
    
    
	// detaching all the various shader stages as there no longer needed after linkng
	// also allows for shader deletion later, shaders don't delete even when its called
	// unless they're detached from all shader programs or the shader program there
	// attached to deletes!
	for (size_t i = 0; i < m_attachedShaders.size(); ++i)
		glDetachShader(m_shaderProgramID, m_attachedShaders[i]);

	// data no longer required
	m_attachedShaders.clear();
	m_attribList.clear();
	m_fragOutputList.clear();
}
