﻿#include "GLSLShader.h"

#include <iostream>
#include <fstream>

GLSLShader::GLSLShader(const char* fileName, ShaderType type)
{
	
	// create the shader id based off of the passed in ShaderType
	switch (type){
	case VERTEX: 
		m_shaderID = glCreateShader(GL_VERTEX_SHADER);
		break;
	case FRAGMENT:
		m_shaderID = glCreateShader(GL_FRAGMENT_SHADER);
		break;
	case GEOMETRY:
		m_shaderID = glCreateShader(GL_GEOMETRY_SHADER);
		break;
	default:
		std::cout << "Incompatible Shader Type or Inccorrect Type Name" << std::endl;
		break;
	}

	// loads in the shader from the specified filename and then compiles it
	// the filename and type should be compatible with each other otherwise 
	// it'll throw errors (even if the shader is valid syntax for another shader type)
	initShader(loadShaderFile(fileName));

}

GLSLShader::~GLSLShader()
{
	glDeleteShader(m_shaderID); // deletes the shader from the GPU, otherwise it'd just be chilling in memory
}

// loads in a file from a basic text file, can be anything but its 
// intention here is for loading in shader source code
std::string GLSLShader::loadShaderFile(const char* fileName) const
{
	std::fstream fs;
	std::string tempS;

	fs.open(fileName); // open file

	if (fs.is_open()) // check if its open
	{
		while (!fs.eof()) // stop when its the end of the file
		{
			char c = fs.get(); // get the next char from the file

			// stops appending if we get any file issues stops garbage characters
			// being appended.
			if (fs.good()) 
				tempS += c; // append to string
		}

		fs.close(); // close the file
	}
	else
		std::cout << "Shader file could not be opened" << std::endl;

	return tempS; 
}

// passes the shader source code into the created shader and then compiles it and checks for errors
void GLSLShader::initShader(std::string shaderSource)
{
	const GLchar* data = shaderSource.data();
	const GLint size = shaderSource.size();
	glShaderSource(m_shaderID, 1, &data, &size); // pass shader source to the shader with the specified ID

	glCompileShader(m_shaderID); // compile the shader source code in the shader ID

	GLint compiled;
	glGetShaderiv(m_shaderID, GL_COMPILE_STATUS, &compiled); // check its current compilation status
	
	// if it didn't compile, check the errors
	if (!compiled) 
		std::cout << "Shader did not compile." << std::endl;
		
	
}

