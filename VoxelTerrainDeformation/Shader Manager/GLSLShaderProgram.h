#pragma once

#include <map>
#include <vector>
#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>


// GLSL shader class contains the current shader program stored witin alongside functions to interact and send variables to the shader stored within
// USED IN EXTERNAL PROJECT
class GLSLShaderProgram{

public:
	GLSLShaderProgram(){}
	~GLSLShaderProgram();
	GLuint getShaderID() const {return m_shaderProgramID;}      // returns current shaderID
	GLuint getUniformLocation(const char * uniformName) const;  // returns the uniform location of a specified uniform variable when you pass in its name, good for error checking -1 means it's non-existant and eliminating repetitive code

	void use() const { glUseProgram(m_shaderProgramID); }	// sets the shader held within this shader object as the current active shader in use by OpenGL
		
	// sends various numbers of floating point data to the Shader Program, floats, vec2s, vec3s, vec4s etc.
	void setUniform1f(const char* uniformName, GLfloat data) const;
	void setUniform2fv(const char* uniformName, int count, const GLfloat* data) const;
	void setUniform3fv(const char* uniformName, int count, const GLfloat* data) const;
	void setUniform4fv(const char* uniformName, int count, const GLfloat* data) const;
	
	// passes in matrices to the Shader Program, 3x3/4x4 currenlty. Can transpose it by passing in true to transpose
	// not normally required.
	void setUniformMatrix3fv(const char* uniformName, int count, GLboolean transpose, const GLfloat* data) const;
	void setUniformMatrix4fv(const char* uniformName, int count, GLboolean transpose, const GLfloat* data) const;             

	void setUniformi(const char* uniformName, GLuint data) const;             // sends an integer to the shader


	// Don't have to specify the various attributes or inputs explicitly if they're defined in the shader, 
	// they must all be specified Pre-Compilation. 
	// However I'd 100% advise doing it, shaders have a quirky tendency to specify the wrong locations given the chance

	// for Vertex input attributes, can also specify in-shader via layout (location = 0) in vec3 name etc. 
	void addAttribute(GLuint index, const char* attribName); // add an attribute to the attribute list that will get used at shader creation
	void addDefaultAttributes();								   // creates the default attribute list texCoords, positions, normals.
	void clearAttributes();										   // clears the current attribute list

	// for fragment output colours, same as input attributes  
	void addFragOutput(GLuint index, const char* outputName);
	void addDefaultOutput();
	void clearFragOutput();

	void attachShader(GLuint shaderID);

	void compileShaderProgram();

private:
	GLuint m_shaderProgramID; // holds the ID for the shader
	std::map<const GLuint, const char*> m_attribList;
	std::map<const GLuint, const char*> m_fragOutputList;
	std::vector<GLuint> m_attachedShaders;
};
