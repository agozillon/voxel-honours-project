#include "DualContouring.h"
#include "../Noise Generators/PerlinNoise.h"

#include <queue>
#include <iostream>


bool DualContouring::signChange(float edgeDensity1, float edgeDensity2)
{
	if ((edgeDensity1 < 0) != (edgeDensity2 < 0) || edgeDensity1 == 0 || edgeDensity2 == 0)
		return true;
	else
		return false;
}

// we use the Eigen Library for matrix calculations it has a far superior
// matrix calculation library in comparison to GLM (Jacobian Rotation, SVD & QR Decomposition and much more)
// It's also a SIMD/Multicore template library, it'll be deactivated for research purposes but interesting for later applications
// anyway!

// Singular Value Decomposition (SVD) QEF, it's numerically unstable in comparison to the 
// other method the Dual Contouring paper describes (QR Decomposition) but is 50 calculations less
// and thus faster (as mentioned by the Dual Contouring Hermite Data paper) so worth the trade off as marginal vertex misplacements 
// are manageable  
Eigen::Vector3f DualContouring::calculateQEFSVD(std::vector<Eigen::Vector3f> signChangeNormals, std::vector<Eigen::Vector3f> signChangePoints)
{

	// dealing with full cube
	Eigen::MatrixXf A(signChangeNormals.size(), 3);
	std::vector<float> B;
	Eigen::Vector3f massPoint(0, 0, 0);

	for (size_t i = 0; i < signChangeNormals.size(); ++i)
	{
		A(i, 0) = signChangeNormals[i][0];
		A(i, 1) = signChangeNormals[i][1];
		A(i, 2) = signChangeNormals[i][2];
		
		massPoint += signChangePoints[i];
	}

	massPoint /= (float)signChangePoints.size(); // the massPoint is essentially the averaging of all the intersection points

	for (size_t i = 0; i < signChangePoints.size(); ++i)
	{
		B.push_back(signChangeNormals[i].dot(signChangePoints[i] - massPoint)); // compute dot product for B Matrix
	}
		
	// may be able to replace this with one command eventually
	Eigen::MatrixXf U = A.jacobiSvd(Eigen::ComputeThinU).matrixU();
	Eigen::MatrixXf V = A.jacobiSvd(Eigen::ComputeThinV).matrixV();
	Eigen::MatrixXf D = A.jacobiSvd().singularValues();

	Eigen::Vector3f tempP(0, 0, 0);

	for (size_t i = 0; i < signChangePoints.size(); ++i) // row
	{
		if (B[i] != 0.0)
		{
			for (int j = 0; j < 3; ++j) // column
			{
				tempP(j) += U(i, j) * B[i];
			}
		}
	}
	
	for (int i = 0; i < 3; ++i)
	{
		if (D(i) > 0.1)
			tempP(i) /= D(i);
	}
	
	Eigen::Vector3f coordValue(0, 0, 0);

	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			coordValue[i] += tempP(j) * V(i, j);

		return coordValue + massPoint;
}



float percentOfLine(float a, float b)
{
	float t = abs(a) + abs(b);
	float l = (t - abs(b)) / t;
	return l; 
}

/*
	Sets up Dual Contouring Class with classes required for rendring
	@param int dimensionOfTree - the dimension of the tree in cubes i.e 16x16x16 (each cube is 2 units big so technically it takes up a 32 unit range)
	we work out the depth from this
*/
DualContouring::DualContouring(std::shared_ptr<CameraManager> cameraManager, std::shared_ptr<ShaderManager> shaderManager, std::shared_ptr<PerlinNoise> perlinNoiseGen, std::shared_ptr<Texture2D> texture)
: m_cameraManager(cameraManager), m_shaderManager(shaderManager), m_perlinNoise(perlinNoiseGen), m_texture(texture)
{
	
}


void DualContouring::removeDensityData(glm::vec3 indexVal, int dimensionOfData, std::vector<std::vector<std::vector<float>>> densityField)
{
	int startPoint = -dimensionOfData;

	// 1 = inside, -1 = outside
	m_voxels[indexVal.x][indexVal.y][indexVal.z].densities[7] = -1; // center density and all the changes on itl across all voxels
	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].densities[3] = -1;
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].densities[6] = -1;
	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].densities[2] = -1;
	m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].densities[5] = -1;
	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].densities[0] = -1;
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].densities[4] = -1;
	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].densities[1] = -1;

	m_voxels[indexVal.x][indexVal.y][indexVal.z].hasChanged = true; // center density and all the changes on itl across all voxels
	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].hasChanged = true;
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].hasChanged = true;
	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].hasChanged = true;
	m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].hasChanged = true;
	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].hasChanged = true;
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].hasChanged = true;
	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].hasChanged = true;
	
	for (int i = 0; i < 8; ++i)
	{
		if (i != 0 && m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].densities[i] <= 0)
			m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].densities[i] = 1;

		if (i != 1 && m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].densities[i] <= 0)
			m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].densities[1] = 1;

		if (i != 2 && m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].densities[i] <= 0)
			m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].densities[i] = 1;

		if (i != 3 && m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].densities[i] <= 0)
			m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].densities[i] = 1;
		
		if (i != 4 && m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].densities[i] <= 0)
			m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].densities[i] = 1;

		if (i != 5 && m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].densities[i] <= 0)
			m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].densities[i] = 1;
	
		if (i != 6 && m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].densities[i] <= 0)
			m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].densities[i] = 1;

		if (i != 7 && m_voxels[indexVal.x][indexVal.y][indexVal.z].densities[i] <= 0)
			m_voxels[indexVal.x][indexVal.y][indexVal.z].densities[i] = 1;
	}

	m_voxels[indexVal.x][indexVal.y][indexVal.z].edgeNormals[9] = Eigen::Vector3f(1, 0, 0);
	m_voxels[indexVal.x][indexVal.y][indexVal.z].edgeNormals[10] = Eigen::Vector3f(0, -1, 0);
	m_voxels[indexVal.x][indexVal.y][indexVal.z].edgeNormals[5] = Eigen::Vector3f(0, 0, -1);

	m_voxels[indexVal.x][indexVal.y][indexVal.z].isEdgeSurface[9] = signChange(m_voxels[indexVal.x][indexVal.y][indexVal.z].densities[3],
		m_voxels[indexVal.x][indexVal.y][indexVal.z].densities[7]);
	m_voxels[indexVal.x][indexVal.y][indexVal.z].isEdgeSurface[10] = signChange(m_voxels[indexVal.x][indexVal.y][indexVal.z].densities[6],
		m_voxels[indexVal.x][indexVal.y][indexVal.z].densities[7]);
	m_voxels[indexVal.x][indexVal.y][indexVal.z].isEdgeSurface[5] = signChange(m_voxels[indexVal.x][indexVal.y][indexVal.z].densities[5],
		m_voxels[indexVal.x][indexVal.y][indexVal.z].densities[7]);
	/*
	calculatePerlinIntersection(m_voxels[indexVal.x][indexVal.y][indexVal.z].densities[3],
		m_voxels[indexVal.x][indexVal.y][indexVal.z].densities[7]);
	*/

	m_voxels[indexVal.x][indexVal.y][indexVal.z].edgeIntersections[9] = Eigen::Vector3f(startPoint + (indexVal.x * 2), startPoint + (indexVal.y * 2), startPoint + (indexVal.z * 2));
	m_voxels[indexVal.x][indexVal.y][indexVal.z].edgeIntersections[10] = Eigen::Vector3f(startPoint + (indexVal.x * 2), startPoint + (indexVal.y * 2), startPoint + (indexVal.z * 2));
	m_voxels[indexVal.x][indexVal.y][indexVal.z].edgeIntersections[5] = Eigen::Vector3f(startPoint + (indexVal.x * 2), startPoint + (indexVal.y * 2), startPoint + (indexVal.z * 2));




	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].edgeNormals[9] = Eigen::Vector3f(-1, 0, 0);
	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].edgeNormals[8] = Eigen::Vector3f(0, -1, 0);
	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].edgeNormals[4] = Eigen::Vector3f(0, 0, -1);

	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].isEdgeSurface[9] = signChange(m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].densities[3],
		m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].densities[7]);
	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].isEdgeSurface[8] = signChange(m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].densities[2],
		m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].densities[3]);
	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].isEdgeSurface[4] = signChange(m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].densities[3],
		m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].densities[0]);

	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].edgeIntersections[9] = Eigen::Vector3f(startPoint + ((indexVal.x + 1) * 2), startPoint + (indexVal.y * 2), startPoint + (indexVal.z * 2));
	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].edgeIntersections[8] = Eigen::Vector3f(startPoint + ((indexVal.x + 1) * 2), startPoint + (indexVal.y * 2), startPoint + (indexVal.z * 2));
	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z].edgeIntersections[4] = Eigen::Vector3f(startPoint + ((indexVal.x  + 1) * 2), startPoint + (indexVal.y * 2), startPoint + (indexVal.z * 2));




	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].edgeNormals[11] = Eigen::Vector3f(1, 0, 0);
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].edgeNormals[10] = Eigen::Vector3f(0, 1, 0);
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].edgeNormals[6] = Eigen::Vector3f(0, 0, -1);
	
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].isEdgeSurface[11] = signChange(m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].densities[2],
		m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].densities[6]);
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].isEdgeSurface[10] = signChange(m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].densities[6],
		m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].densities[7]);
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].isEdgeSurface[6] = signChange(m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].densities[6],
		m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].densities[4]);
	
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].edgeIntersections[11] = Eigen::Vector3f(startPoint + (indexVal.x * 2), startPoint + ((indexVal.y - 1) * 2), startPoint + (indexVal.z * 2));
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].edgeIntersections[10] = Eigen::Vector3f(startPoint + (indexVal.x * 2), startPoint + ((indexVal.y - 1) * 2), startPoint + (indexVal.z * 2));
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z].edgeIntersections[6] = Eigen::Vector3f(startPoint + (indexVal.x * 2), startPoint + ((indexVal.y - 1) * 2), startPoint + (indexVal.z * 2));



	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].edgeNormals[11] = Eigen::Vector3f(-1, 0, 0);
	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].edgeNormals[8] = Eigen::Vector3f(0, 1, 0);
	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].edgeNormals[7] = Eigen::Vector3f(0, 0, -1);

	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].isEdgeSurface[11] = signChange(m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].densities[2],
		m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].densities[6]);
	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].isEdgeSurface[7] = signChange(m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].densities[1],
		m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].densities[2]);
	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].isEdgeSurface[8] = signChange(m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].densities[2],
		m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].densities[3]);

	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].edgeIntersections[11] = Eigen::Vector3f(startPoint + ((indexVal.x + 1) * 2), startPoint + ((indexVal.y - 1) * 2), startPoint + (indexVal.z * 2));
	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].edgeIntersections[8] = Eigen::Vector3f(startPoint + ((indexVal.x + 1) * 2), startPoint + ((indexVal.y - 1) * 2), startPoint + (indexVal.z * 2));
	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z].edgeIntersections[7] = Eigen::Vector3f(startPoint + ((indexVal.x + 1) * 2), startPoint + ((indexVal.y - 1) * 2), startPoint + (indexVal.z * 2));
	



	m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].edgeNormals[1] = Eigen::Vector3f(1, 0, 0);
	m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].edgeNormals[2] = Eigen::Vector3f(0, -1, 0);
	m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].edgeNormals[5] = Eigen::Vector3f(0, 0, 1);

	m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].isEdgeSurface[1] = signChange(m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].densities[5],
		m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].densities[0]);
	m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].isEdgeSurface[2] = signChange(m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].densities[5],
		m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].densities[4]);
	m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].isEdgeSurface[5] = signChange(m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].densities[5],
		m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].densities[7]);

	m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].edgeIntersections[1] = Eigen::Vector3f(startPoint + (indexVal.x * 2), startPoint + (indexVal.y * 2), startPoint + ((indexVal.z + 1) * 2));
	m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].edgeIntersections[2] = Eigen::Vector3f(startPoint + (indexVal.x * 2), startPoint + (indexVal.y * 2), startPoint + ((indexVal.z + 1) * 2));
	m_voxels[indexVal.x][indexVal.y][indexVal.z + 1].edgeIntersections[5] = Eigen::Vector3f(startPoint + (indexVal.x * 2), startPoint + (indexVal.y * 2), startPoint + ((indexVal.z + 1) * 2));
	
	
	
	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].edgeNormals[0] = Eigen::Vector3f(-1, 0, 0);
	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].edgeNormals[1] = Eigen::Vector3f(0, -1, 0);
	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].edgeNormals[4] = Eigen::Vector3f(0, 0, 1);

	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].isEdgeSurface[0] = signChange(m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].densities[0],
		m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].densities[1]);
	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].isEdgeSurface[1] = signChange(m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].densities[0],
		m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].densities[5]);
	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].isEdgeSurface[4] = signChange(m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].densities[0],
		m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].densities[3]);

	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].edgeIntersections[0] = Eigen::Vector3f(startPoint + ((indexVal.x + 1) * 2), startPoint + (indexVal.y * 2), startPoint + ((indexVal.z + 1) * 2));
	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].edgeIntersections[1] = Eigen::Vector3f(startPoint + ((indexVal.x + 1) * 2), startPoint + (indexVal.y * 2), startPoint + ((indexVal.z + 1) * 2));
	m_voxels[indexVal.x + 1][indexVal.y][indexVal.z + 1].edgeIntersections[4] = Eigen::Vector3f(startPoint + ((indexVal.x + 1) * 2), startPoint + (indexVal.y * 2), startPoint + ((indexVal.z + 1) * 2));




	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].edgeNormals[3] = Eigen::Vector3f(1, 0, 0);
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].edgeNormals[2] = Eigen::Vector3f(0, 1, 0);
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].edgeNormals[6] = Eigen::Vector3f(0, 0, 1);
	
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].isEdgeSurface[3] = signChange(m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].densities[4],
		m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].densities[1]);
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].isEdgeSurface[2] = signChange(m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].densities[4],
		m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].densities[5]);
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].isEdgeSurface[6] = signChange(m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].densities[4],
		m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].densities[6]);
	
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].edgeIntersections[3] = Eigen::Vector3f(startPoint + (indexVal.x * 2), startPoint + ((indexVal.y - 1) * 2), startPoint + ((indexVal.z + 1) * 2));
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].edgeIntersections[2] = Eigen::Vector3f(startPoint + (indexVal.x * 2), startPoint + ((indexVal.y - 1) * 2), startPoint + ((indexVal.z + 1) * 2));
	m_voxels[indexVal.x][indexVal.y - 1][indexVal.z + 1].edgeIntersections[6] = Eigen::Vector3f(startPoint + (indexVal.x * 2), startPoint + ((indexVal.y - 1) * 2), startPoint + ((indexVal.z + 1) * 2));

	
	
	
	//
	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].edgeNormals[3] = Eigen::Vector3f(-1, 0, 0);
	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].edgeNormals[0] = Eigen::Vector3f(0, 1, 0);
	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].edgeNormals[7] = Eigen::Vector3f(0, 0, 1);

	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].isEdgeSurface[3] = signChange(m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].densities[4],
		m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].densities[1]);
	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].isEdgeSurface[0] = signChange(m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].densities[0],
		m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].densities[1]);
	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].isEdgeSurface[7] = signChange(m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].densities[1],
		m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].densities[2]);

	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].edgeIntersections[3] = Eigen::Vector3f(startPoint + ((indexVal.x + 1) * 2), startPoint + ((indexVal.y - 1) * 2), startPoint + ((indexVal.z + 1) * 2));
	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].edgeIntersections[0] = Eigen::Vector3f(startPoint + ((indexVal.x + 1) * 2), startPoint + ((indexVal.y - 1) * 2), startPoint + ((indexVal.z + 1) * 2));
	m_voxels[indexVal.x + 1][indexVal.y - 1][indexVal.z + 1].edgeIntersections[7] = Eigen::Vector3f(startPoint + ((indexVal.x + 1) * 2), startPoint + ((indexVal.y - 1) * 2), startPoint + ((indexVal.z + 1) * 2));

	generateFromData();
	
	/*
	densityField[indexVal.x][indexVal.y][indexVal.z] = 2; // bottom left
	densityField[indexVal.x][indexVal.y + 1][indexVal.z] = 2; // top left
	densityField[indexVal.x][indexVal.y][indexVal.z + 1] = 2; // back bottom left
	densityField[indexVal.x][indexVal.y + 1][indexVal.z + 1] = 2; // back top left
	densityField[indexVal.x + 1][indexVal.y][indexVal.z] = 2; // bottom right
	densityField[indexVal.x + 1][indexVal.y + 1][indexVal.z] = 2; // top right
	densityField[indexVal.x + 1][indexVal.y][indexVal.z + 1] = 2; // back bottom right
	densityField[indexVal.x + 1][indexVal.y + 1][indexVal.z + 1] = 2; // back top right 
	*/

	//constructSurface(dimensionOfData, densityField);
}

void DualContouring::generateFromData()
{
	int i2 = 0;
	int startPoint = -m_dimensionData;
	std::vector<Eigen::Vector3f> normals;
	std::vector<Eigen::Vector3f> intersectionPoints;
	std::vector<std::vector<std::vector<Eigen::Vector3f>>> verts = std::vector<std::vector<std::vector<Eigen::Vector3f>>>(m_dimensionData + 1,
		std::vector<std::vector<Eigen::Vector3f>>(m_dimensionData + 1,
		std::vector<Eigen::Vector3f>(m_dimensionData + 1)));
	Eigen::Vector3f tmpNormal(0, 0, 0), tmpIntersection(0, 0, 0);

	int numberOfCubes = 0;

	for (int i = 0; i < m_voxels.size(); ++i)
	{
		for (int j = 0; j < m_voxels.size(); ++j)
		{
			for (int k = 0; k < m_voxels.size(); ++k)
			{
					if (m_voxels[i][j][k].hasChanged == false && m_voxels[i][j][k].hasSurface == true)
					{
						verts[i][j][k] = m_voxels[i][j][k].vertex;
						
						numberOfCubes++;
					}
					else
					{
						m_voxels[i][j][k].hasChanged = false;

						for (i2 = 0; i2 < 12; ++i2)
						{
							if (m_voxels[i][j][k].isEdgeSurface[i2])
							{
								normals.push_back(m_voxels[i][j][k].edgeNormals[i2]);
								intersectionPoints.push_back(m_voxels[i][j][k].edgeIntersections[i2]);
								std::cout << "Edge Intersect X:  " << m_voxels[i][j][k].edgeIntersections[i2](0) << "Edge Intersect Y: " << m_voxels[i][j][k].edgeIntersections[i2](1) << "Edge Intersect Z: " << m_voxels[i][j][k].edgeIntersections[i2](2) << std::endl;
							}
						}

						if (intersectionPoints.size() > 0 && normals.size() > 0)
						{
							verts[i][j][k] = m_voxels[i][j][k].vertex = calculateQEFSVD(normals, intersectionPoints);
							m_voxels[i][j][k].hasSurface = true;
						}

						normals.clear();
						intersectionPoints.clear();
					}
			}
		}
	}

	std::cout << "Cubes 2: " << numberOfCubes << std::endl;
	createMesh(m_vao, m_indexCount, verts);
}


void DualContouring::constructSurface(int dimensionOfData, std::vector<std::vector<std::vector<float>>> densityField, float voxelSize)
{

	m_voxels = std::vector<std::vector<std::vector<VoxelData>>>(densityField.size() - 1, std::vector<std::vector<VoxelData>>(densityField[0].size() - 1, std::vector<VoxelData>(densityField[0][0].size() - 1)));
	m_voxelSize = voxelSize;

	int numberOfVertices = 0;
	int startPoint = -dimensionOfData; // calculating the start point of the grid by negating half the size of the grid by the center
	m_dimensionData = dimensionOfData;
	std::vector<Eigen::Vector3f> normals;
	std::vector<Eigen::Vector3f> intersectionPoints;
	std::vector<std::vector<std::vector<Eigen::Vector3f>>> verts = std::vector<std::vector<std::vector<Eigen::Vector3f>>>(densityField.size() - 1,
		std::vector<std::vector<Eigen::Vector3f>>(densityField[0].size() - 1,
		std::vector<Eigen::Vector3f>(densityField[0][0].size() - 1)));

	Eigen::Vector3f tmpNormal(0, 0, 0), tmpIntersection(0, 0, 0);

	for (int i = 0; i < densityField.size() - 1; ++i)
	{
		for (int j = 0; j < densityField[i].size() - 1; ++j)
		{
			for (int k = 0; k < densityField[i][j].size() - 1; ++k)
			{
				// compute edge 1
				if (signChange(densityField[i][j][k], densityField[i][j + 1][k]))
				{		
					tmpIntersection(0) = startPoint + (i * m_voxelSize);
					tmpIntersection(1) = startPoint + (j * m_voxelSize) + (m_voxelSize * percentOfLine(densityField[i][j][k], densityField[i][j + 1][k]));
					tmpIntersection(2) = startPoint + (k * m_voxelSize);

					m_perlinNoise->fractionalBrownianMotionAndGradient(glm::vec3(tmpIntersection(0) / 33, tmpIntersection(1) / 33, tmpIntersection(2) / 33), glm::vec3(2.5f, 2.5f, 2.5f), 3.5f, 0.5f, 0.95, 1, tmpNormal, false);

					normals.push_back(tmpNormal);
					intersectionPoints.push_back(tmpIntersection);

					m_voxels[i][j][k].densities[0] = densityField[i][j][k];
					m_voxels[i][j][k].densities[2] = densityField[i][j + 1][k];
					m_voxels[i][j][k].isEdgeSurface[0] = true;
					m_voxels[i][j][k].edgeNormals[0] = tmpNormal;
					m_voxels[i][j][k].edgeIntersections[0] = tmpIntersection;
				}
				else
				{
					m_voxels[i][j][k].densities[0] = densityField[i][j][k];
					m_voxels[i][j][k].densities[2] = densityField[i][j + 1][k];
					m_voxels[i][j][k].isEdgeSurface[0] = false;
				}

				// compute edge 2
				if (signChange(densityField[i][j][k], densityField[i + 1][j][k]))
				{


					tmpIntersection(0) = startPoint + (i * m_voxelSize) + (m_voxelSize * percentOfLine(densityField[i][j][k], densityField[i + 1][j][k]));
					tmpIntersection(1) = startPoint + (j * m_voxelSize);
					tmpIntersection(2) = startPoint + (k * m_voxelSize);

					m_perlinNoise->fractionalBrownianMotionAndGradient(glm::vec3(tmpIntersection(0) / 33, tmpIntersection(1) / 33, tmpIntersection(2) / 33), glm::vec3(2.5f, 2.5f, 2.5f), 3.5f, 0.5f, 0.95, 1, tmpNormal, false);

					normals.push_back(tmpNormal);
					intersectionPoints.push_back(tmpIntersection);

					m_voxels[i][j][k].densities[5] = densityField[i + 1][j][k];
					m_voxels[i][j][k].isEdgeSurface[1] = true;
					m_voxels[i][j][k].edgeNormals[1] = tmpNormal;
					m_voxels[i][j][k].edgeIntersections[1] = tmpIntersection;
				}
				else
				{
					m_voxels[i][j][k].densities[5] = densityField[i + 1][j][k];
					m_voxels[i][j][k].isEdgeSurface[1] = false;
				}

				// compute edge 3
				if (signChange(densityField[i + 1][j][k], densityField[i + 1][j + 1][k]))
				{
					tmpIntersection(0) = startPoint + (i * m_voxelSize) + m_voxelSize;
					tmpIntersection(1) = startPoint + (j * m_voxelSize) + (m_voxelSize * percentOfLine(densityField[i + 1][j][k], densityField[i + 1][j + 1][k]));
					tmpIntersection(2) = startPoint + (k * m_voxelSize);

					m_perlinNoise->fractionalBrownianMotionAndGradient(glm::vec3(tmpIntersection(0) / 33, tmpIntersection(1) / 33, tmpIntersection(2) / 33), glm::vec3(2.5f, 2.5f, 2.5f), 3.5f, 0.5f, 0.95, 1, tmpNormal, false);

					normals.push_back(tmpNormal);
					intersectionPoints.push_back(tmpIntersection);

					m_voxels[i][j][k].densities[4] = densityField[i + 1][j + 1][k];
					m_voxels[i][j][k].isEdgeSurface[2] = true;
					m_voxels[i][j][k].edgeNormals[2] = tmpNormal;
					m_voxels[i][j][k].edgeIntersections[2] = tmpIntersection;
				}
				else
				{
					m_voxels[i][j][k].densities[4] = densityField[i + 1][j + 1][k];
					m_voxels[i][j][k].isEdgeSurface[2] = false;
				}

				// compute edge 4
				if (signChange(densityField[i + 1][j + 1][k], densityField[i][j + 1][k]))
				{

					tmpIntersection(0) = startPoint + (i * m_voxelSize) + (m_voxelSize * percentOfLine(densityField[i][j + 1][k], densityField[i + 1][j + 1][k]));
					tmpIntersection(1) = startPoint + (j * m_voxelSize) + m_voxelSize;
					tmpIntersection(2) = startPoint + (k * m_voxelSize);

					m_perlinNoise->fractionalBrownianMotionAndGradient(glm::vec3(tmpIntersection(0) / 33, tmpIntersection(1) / 33, tmpIntersection(2) / 33), glm::vec3(2.5f, 2.5f, 2.5f), 3.5f, 0.5f, 0.95, 1, tmpNormal , false);

					normals.push_back(tmpNormal);
					intersectionPoints.push_back(tmpIntersection);

					m_voxels[i][j][k].isEdgeSurface[3] = true;
					m_voxels[i][j][k].edgeNormals[3] = tmpNormal;
					m_voxels[i][j][k].edgeIntersections[3] = tmpIntersection;
				}
				else
				{
					m_voxels[i][j][k].isEdgeSurface[3] = false;
				}

				// compute edge 5
				if (signChange(densityField[i][j][k], densityField[i][j][k + 1]))
				{
					tmpIntersection(0) = startPoint + (i * m_voxelSize);
					tmpIntersection(1) = startPoint + (j * m_voxelSize);
					tmpIntersection(2) = startPoint + (k * m_voxelSize) + (m_voxelSize * percentOfLine(densityField[i][j][k], densityField[i][j][k + 1]));
					
					m_perlinNoise->fractionalBrownianMotionAndGradient(glm::vec3(tmpIntersection(0) / 33, tmpIntersection(1) / 33, tmpIntersection(2) / 33), glm::vec3(2.5f, 2.5f, 2.5f), 3.5f, 0.5f, 0.95, 1, tmpNormal, false);

					normals.push_back(tmpNormal);
					intersectionPoints.push_back(tmpIntersection);

					m_voxels[i][j][k].densities[3] = densityField[i][j][k + 1];
					m_voxels[i][j][k].isEdgeSurface[4] = true;
					m_voxels[i][j][k].edgeNormals[4] = tmpNormal;
					m_voxels[i][j][k].edgeIntersections[4] = tmpIntersection;
				}
				else
				{
					m_voxels[i][j][k].densities[3] = densityField[i][j][k + 1];
					m_voxels[i][j][k].isEdgeSurface[4] = false;
				}

				// compute edge 6
				if (signChange(densityField[i + 1][j][k], densityField[i + 1][j][k + 1]))
				{

					tmpIntersection(0) = startPoint + (i * m_voxelSize) + m_voxelSize;
					tmpIntersection(1) = startPoint + (j * m_voxelSize);
					tmpIntersection(2) = startPoint + (k * m_voxelSize) + (m_voxelSize * percentOfLine(densityField[i + 1][j][k], densityField[i + 1][j][k + 1]));

					m_perlinNoise->fractionalBrownianMotionAndGradient(glm::vec3(tmpIntersection(0) / 33, tmpIntersection(1) / 33, tmpIntersection(2) / 33), glm::vec3(2.5f, 2.5f, 2.5f), 3.5f, 0.5f, 0.95, 1, tmpNormal, false);

					normals.push_back(tmpNormal);
					intersectionPoints.push_back(tmpIntersection);

					m_voxels[i][j][k].densities[7] = densityField[i + 1][j][k + 1];
					m_voxels[i][j][k].isEdgeSurface[5] = true;
					m_voxels[i][j][k].edgeNormals[5] = tmpNormal;
					m_voxels[i][j][k].edgeIntersections[5] = tmpIntersection;
				}
				else
				{
					m_voxels[i][j][k].densities[7] = densityField[i + 1][j][k + 1];
					m_voxels[i][j][k].isEdgeSurface[5] = false;
				}

				// compute edge 7
				if (signChange(densityField[i + 1][j + 1][k], densityField[i + 1][j + 1][k + 1]))
				{
					tmpIntersection(0) = startPoint + (i * m_voxelSize) + m_voxelSize;
					tmpIntersection(1) = startPoint + (j * m_voxelSize) + m_voxelSize;
					tmpIntersection(2) = startPoint + (k * m_voxelSize) + (m_voxelSize * percentOfLine(densityField[i + 1][j + 1][k], densityField[i + 1][j + 1][k + 1]));

					m_perlinNoise->fractionalBrownianMotionAndGradient(glm::vec3(tmpIntersection(0) / 33, tmpIntersection(1) / 33, tmpIntersection(2) / 33), glm::vec3(2.5f, 2.5f, 2.5f), 3.5f, 0.5f, 0.95, 1, tmpNormal, false);

					normals.push_back(tmpNormal);
					intersectionPoints.push_back(tmpIntersection);

					m_voxels[i][j][k].densities[7] = densityField[i + 1][j + 1][k + 1];
					m_voxels[i][j][k].isEdgeSurface[6] = true;
					m_voxels[i][j][k].edgeNormals[6] = tmpNormal;
					m_voxels[i][j][k].edgeIntersections[6] = tmpIntersection;

				}
				else
				{
					m_voxels[i][j][k].densities[7] = densityField[i + 1][j + 1][k + 1];
					m_voxels[i][j][k].isEdgeSurface[6] = false;
				}

				// compute edge 8
				if (signChange(densityField[i][j + 1][k], densityField[i][j + 1][k + 1]))
				{
					tmpIntersection(0) = startPoint + (m_voxelSize * i);
					tmpIntersection(1) = startPoint + (m_voxelSize * j) + m_voxelSize;
					tmpIntersection(2) = startPoint + (m_voxelSize * k) + (m_voxelSize * percentOfLine(densityField[i][j + 1][k], densityField[i][j + 1][k + 1]));

					m_perlinNoise->fractionalBrownianMotionAndGradient(glm::vec3(tmpIntersection(0) / 33, tmpIntersection(1) / 33, tmpIntersection(2) / 33), glm::vec3(2.5f, 2.5f, 2.5f), 3.5f, 0.5f, 0.95, 1, tmpNormal , false);
	
					normals.push_back(tmpNormal);
					intersectionPoints.push_back(tmpIntersection);

					m_voxels[i][j][k].densities[2] = densityField[i][j + 1][k + 1];
					m_voxels[i][j][k].isEdgeSurface[7] = true;
					m_voxels[i][j][k].edgeNormals[7] = tmpNormal;
					m_voxels[i][j][k].edgeIntersections[7] = tmpIntersection;
				}
				else
				{
					m_voxels[i][j][k].densities[2] = densityField[i][j + 1][k + 1];
					m_voxels[i][j][k].isEdgeSurface[7] = false;
				}

				// compute edge 9
				if (signChange(densityField[i][j][k + 1], densityField[i][j + 1][k + 1]))
				{
					tmpIntersection(0) = startPoint + (i * m_voxelSize);
					tmpIntersection(1) = startPoint + (j * m_voxelSize) + (m_voxelSize * percentOfLine(densityField[i][j][k + 1], densityField[i][j + 1][k + 1]));
					tmpIntersection(2) = startPoint + (k * m_voxelSize) + m_voxelSize;

					m_perlinNoise->fractionalBrownianMotionAndGradient(glm::vec3(tmpIntersection(0) / 33, tmpIntersection(1) / 33, tmpIntersection(2) / 33), glm::vec3(2.5f, 2.5f, 2.5f), 3.5f, 0.5f, 0.95, 1, tmpNormal, false);

					normals.push_back(tmpNormal);
					intersectionPoints.push_back(tmpIntersection);

					m_voxels[i][j][k].isEdgeSurface[8] = true;
					m_voxels[i][j][k].edgeNormals[8] = tmpNormal;
					m_voxels[i][j][k].edgeIntersections[8] = tmpIntersection;
				}
				else
				{
					m_voxels[i][j][k].isEdgeSurface[8] = false;
				}

				// compute edge 10
				if (signChange(densityField[i][j][k + 1], densityField[i + 1][j][k + 1]))
				{
					tmpIntersection(0) = startPoint + (i * m_voxelSize) + (m_voxelSize * percentOfLine(densityField[i][j][k + 1], densityField[i + 1][j][k + 1]));
					tmpIntersection(1) = startPoint + (j * m_voxelSize);
					tmpIntersection(2) = startPoint + (k * m_voxelSize) + m_voxelSize;

					m_perlinNoise->fractionalBrownianMotionAndGradient(glm::vec3(tmpIntersection(0) / 33, tmpIntersection(1) / 33, tmpIntersection(2) / 33), glm::vec3(2.5f, 2.5f, 2.5f), 3.5f, 0.5f, 0.95, 1, tmpNormal, false);

					normals.push_back(tmpNormal);
					intersectionPoints.push_back(tmpIntersection);

					m_voxels[i][j][k].isEdgeSurface[9] = true;
					m_voxels[i][j][k].edgeNormals[9] = tmpNormal;
					m_voxels[i][j][k].edgeIntersections[9] = tmpIntersection;
				}
				else
				{
					m_voxels[i][j][k].isEdgeSurface[9] = false;
				}

				// compute edge 11
				if (signChange(densityField[i + 1][j][k + 1], densityField[i + 1][j + 1][k + 1]))
				{
					tmpIntersection(0) = startPoint + (i * m_voxelSize) + m_voxelSize;
					tmpIntersection(1) = startPoint + (j * m_voxelSize) + (m_voxelSize * percentOfLine(densityField[i + 1][j][k + 1], densityField[i + 1][j + 1][k + 1]));
					tmpIntersection(2) = startPoint + (k * m_voxelSize) + m_voxelSize;

					m_perlinNoise->fractionalBrownianMotionAndGradient(glm::vec3(tmpIntersection(0) / 33, tmpIntersection(1) / 33, tmpIntersection(2) / 33), glm::vec3(2.5f, 2.5f, 2.5f), 3.5f, 0.5f, 0.95, 1, tmpNormal, false);

					normals.push_back(tmpNormal);
					intersectionPoints.push_back(tmpIntersection);

					m_voxels[i][j][k].isEdgeSurface[10] = true;
					m_voxels[i][j][k].edgeNormals[10] = tmpNormal;
					m_voxels[i][j][k].edgeIntersections[10] = tmpIntersection;
				}
				else
				{
					m_voxels[i][j][k].isEdgeSurface[10] = false;
				}

				// compute edge 12
				if (signChange(densityField[i + 1][j + 1][k + 1], densityField[i][j + 1][k + 1]))
				{
					tmpIntersection(0) = startPoint + (i * m_voxelSize) + (m_voxelSize * percentOfLine(densityField[i][j + 1][k + 1], densityField[i + 1][j + 1][k + 1]));
					tmpIntersection(1) = startPoint + (j * m_voxelSize) + m_voxelSize;
					tmpIntersection(2) = startPoint + (k * m_voxelSize) + m_voxelSize;

					m_perlinNoise->fractionalBrownianMotionAndGradient(glm::vec3(tmpIntersection(0) / 33, tmpIntersection(1) / 33, tmpIntersection(2) / 33), glm::vec3(2.5f, 2.5f, 2.5f), 3.5f, 0.5f, 0.95, 1, tmpNormal, false);

					normals.push_back(tmpNormal);
					intersectionPoints.push_back(tmpIntersection);

					m_voxels[i][j][k].isEdgeSurface[11] = true;
					m_voxels[i][j][k].edgeNormals[11] = tmpNormal;
					m_voxels[i][j][k].edgeIntersections[11] = tmpIntersection;
				}
				else
				{
					m_voxels[i][j][k].isEdgeSurface[11] = false;
				}


				if (normals.size() != 0 && intersectionPoints.size() != 0)
				{
					m_voxels[i][j][k].hasSurface = true;
					m_voxels[i][j][k].vertex = verts[i][j][k] = calculateQEFSVD(normals, intersectionPoints);
					numberOfVertices++;
				}

				normals.clear();
				intersectionPoints.clear();
			}
		}
	}

	createMesh(m_vao, m_indexCount, verts);
	std::cout << "Number of Vertices: " << numberOfVertices << std::endl;
}

void DualContouring::renderWorld()
{
	m_texture->bindTexture(GL_TEXTURE0);
	// Sets the shader to Textured Shader (doesn't support other shader types at the moment)
	// not hugelly difficult to extend to.
	m_shaderManager->getShaderProgram("Textured Shader")->use();

	// model * view 
	glm::mat4 mv = m_cameraManager->getView() * mat4(1.0);

	m_shaderManager->getShaderProgram("Textured Shader")->setUniformMatrix4fv("modelview", 1, false, glm::value_ptr(mv));
	m_shaderManager->getShaderProgram("Textured Shader")->setUniformMatrix4fv("projection", 1, false, glm::value_ptr(m_cameraManager->getProjection()));

	glBindVertexArray(m_vao);
	glDrawArrays(GL_TRIANGLES, 0, m_indexCount);
}


void DualContouring::createMesh(GLuint &vao, GLuint &indexCount, std::vector<std::vector<std::vector<Eigen::Vector3f>>> vertices)
{
	int numberOfPolygons = 0;
	std::vector<float> cubeVector;
	std::vector<float> cubeUV;
	Eigen::Vector3f p1, p2, p3, p4; // 4 points of a quad

	// uvs for mesh creation
	glm::vec2 uv1(0.0f, 1.0f);
	glm::vec2 uv2(0.0f, 0.0f);
	glm::vec2 uv3(1.0f, 1.0f);
	glm::vec2 uv4(1.0f, 0.0f);
	
	// generates a quad (2 polygons), from the 4 surrounding cubes of the intersection
	// only need to check 3 edge intersections due to the way we traverse the 
	// grid, down the z first then move along one x till we reach the end of the row then up
	// so we only ever really need to pay attention to the top right back corner as all other
	// edges will have previously been checked and generated
	for (int i = 0; i < vertices.size() - 1; ++i)
	{
		for (int j = 0; j < vertices[i].size() - 1; ++j)
		{
			for (int k = 0; k < vertices[i][j].size() - 1; ++k)
			{
				p1 = vertices[i][j][k]; // current cube/voxel

				for (int i2 = 0; i2 < 3; ++i2)
				{
					// edge 7
					if (i2 == 0 && m_voxels[i][j][k].isEdgeSurface[6])
					{
						p2 = vertices[i + 1][j][k]; // voxel to the right
						p3 = vertices[i + 1][j + 1][k]; // voxel to the right and up
						p4 = vertices[i][j + 1][k]; // voxel above
					}
					else if (i2 == 1 && m_voxels[i][j][k].isEdgeSurface[11]) // edge 12
					{
						p2 = vertices[i][j][k + 1]; // voxel to the back
						p3 = vertices[i][j + 1][k + 1]; // voxel to above and back
						p4 = vertices[i][j + 1][k]; // voxel above
					}
					else if (i2 == 2 && m_voxels[i][j][k].isEdgeSurface[10]) // edge 11
					{
						p2 = vertices[i + 1][j][k]; // voxel to the right
						p3 = vertices[i + 1][j][k + 1]; // voxel to the right and back
						p4 = vertices[i][j][k + 1]; // voxel to the back
					}
					else
						continue;

				
						// first triangle
						cubeVector.push_back(p3(0)); cubeVector.push_back(p3(1)); cubeVector.push_back(p3(2));
						cubeUV.push_back(uv1[0]); cubeUV.push_back(uv1[1]);

						cubeVector.push_back(p2(0)); cubeVector.push_back(p2(1)); cubeVector.push_back(p2(2));
						cubeUV.push_back(uv2[0]); cubeUV.push_back(uv2[1]);
						
						cubeVector.push_back(p1(0)); cubeVector.push_back(p1(1)); cubeVector.push_back(p1(2));
						cubeUV.push_back(uv3[0]); cubeUV.push_back(uv3[1]);

						// second triangle
						cubeVector.push_back(p4(0)); cubeVector.push_back(p4(1)); cubeVector.push_back(p4(2));
						cubeUV.push_back(uv4[0]); cubeUV.push_back(uv4[1]);

						cubeVector.push_back(p3(0)); cubeVector.push_back(p3(1)); cubeVector.push_back(p3(2));
						cubeUV.push_back(uv1[0]); cubeUV.push_back(uv1[1]);

						cubeVector.push_back(p1(0)); cubeVector.push_back(p1(1)); cubeVector.push_back(p1(2));
						cubeUV.push_back(uv3[0]); cubeUV.push_back(uv3[1]);

						/*
						if it has a different winding (currently bugged so not in use but commented out so I can come back to it)
						// first triangle
						cubeVector.push_back(p2(0)); cubeVector.push_back(p2(1)); cubeVector.push_back(p2(2));
						cubeVector.push_back(p3(0)); cubeVector.push_back(p3(1)); cubeVector.push_back(p3(2));
						cubeVector.push_back(p1(0)); cubeVector.push_back(p1(1)); cubeVector.push_back(p1(2));

						// second triangle
						cubeVector.push_back(p3(0)); cubeVector.push_back(p3(1)); cubeVector.push_back(p3(2));
						cubeVector.push_back(p4(0)); cubeVector.push_back(p4(1)); cubeVector.push_back(p4(2));
						cubeVector.push_back(p1(0)); cubeVector.push_back(p1(1)); cubeVector.push_back(p1(2));
					*/
						numberOfPolygons += 2;
				}
			}
		}
	}

	std::cout << "Number of Polygons: " << numberOfPolygons << std::endl;

	if (cubeVector.size() > 0)
	{
		// bind VAO
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		GLuint vboVerts;
		glGenBuffers(1, &vboVerts);
		glBindBuffer(GL_ARRAY_BUFFER, vboVerts);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GL_FLOAT)* cubeVector.size(), &cubeVector[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		
		GLuint vboUV;
		glGenBuffers(1, &vboUV);
		glBindBuffer(GL_ARRAY_BUFFER, vboUV);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GL_FLOAT)*cubeUV.size(), &cubeUV[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
				
		indexCount = cubeVector.size() / 3;

	}
	else
		std::cout << "No intersections found " << std::endl;

}
