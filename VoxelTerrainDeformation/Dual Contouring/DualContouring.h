#pragma once

#include <memory>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <Eigen/Dense>
#include <Eigen/SVD>
#include <GL\glew.h>

#include "../Camera/CameraManager.h"
#include "../Shader Manager/ShaderManager.h"
#include "../Texture/Texture2D.h"
#include "../Noise Generators/PerlinNoise.h"


// Their will be some slight overlap of data using this structure
// would be of interest to change it to lessen the amount of memory usage in future
struct VoxelData
{
	bool hasChanged;
	bool hasSurface;
	bool isEdgeSurface[12];
	Eigen::Vector3f edgeNormals[12];
	Eigen::Vector3f edgeIntersections[12];
	Eigen::Vector3f vertex;
	float densities[8];

	VoxelData()
	{
		hasChanged = false;
		hasSurface = false;
		for (int i = 0; i < 12; ++i)
		{
			isEdgeSurface[i] = false;
		}
	}
};

class DualContouring {
public:
	DualContouring(std::shared_ptr<CameraManager> cameraManager, std::shared_ptr<ShaderManager> shaderManager, std::shared_ptr<PerlinNoise> perlinNoiseGen, std::shared_ptr<Texture2D> texture);
	Eigen::Vector3f calculateQEFSVD(std::vector<Eigen::Vector3f> signChangeNormals, std::vector<Eigen::Vector3f> signChangePoints);
	void constructSurface(int dimensionOfData, std::vector<std::vector<std::vector<float>>> densityField, float voxelSize);
	void removeDensityData(glm::vec3 indexVal, int dimensionOfData, std::vector<std::vector<std::vector<float>>> densityField);
	void renderWorld();
	~DualContouring(){}

private:
	bool signChange(float edgeDensity1, float edgeDensity2);
	void generateFromData(); // generates from the data already within the class, held in voxel structures for speed up of deformation purposes only at the moment

	// below functions should be merged with the VoxelMerger eventually 
	void createMesh(GLuint &vao, GLuint &indexCount, std::vector<std::vector<std::vector<Eigen::Vector3f>>> vertices);

	std::shared_ptr<CameraManager> m_cameraManager; // keeps pointers to the active managers so they don't have to constantly be passed in via the render call
	std::shared_ptr<ShaderManager> m_shaderManager;
	std::shared_ptr<PerlinNoise> m_perlinNoise; 
	std::shared_ptr<Texture2D> m_texture;

	GLuint m_vao;
	GLuint m_indexCount;
	int m_dimensionData;
	float m_voxelSize; 

	std::vector<std::vector<std::vector<VoxelData>>> m_voxels;
};